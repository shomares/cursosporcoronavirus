import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IMode } from 'src/interface/IMode';
import { Observable, Subscription } from 'rxjs';
import {changeMode} from 'src/app/actions/mode.actions';

@Component({
  selector: 'app-mode-selector',
  templateUrl: './mode-selector.component.html',
  styleUrls: ['./mode-selector.component.scss']
})
export class ModeSelectorComponent implements OnInit {

  mode$ : Observable<IMode>;

  subscription : Subscription;

  constructor(private store : Store< {mode : IMode}>) {
      this.mode$ = store.pipe(
                      select('mode')
                  );

   }


  ChangeMode() : void{

    this.store.dispatch(changeMode());

  }

  ngOnInit(): void {
    this.subscription = this.mode$.subscribe( ({isWhite})=> {
      const root = document.documentElement;

      if(isWhite)
      {
        root.style.setProperty('--primary-color-item', '#f4f5ed');
        root.style.setProperty('--primary-color-item-border', '#e7e8e1');
        root.style.setProperty('--primary-color-item-hover', '#bcbdb8');
        root.style.setProperty('--color-letter', '#4a4a4a');
        root.style.setProperty('--primary-color', 'white');
        
      }else{
        root.style.setProperty('--primary-color-item', '#4a4a4a');
        root.style.setProperty('--primary-color-item-border', '#4a4a4a');
        root.style.setProperty('--primary-color-item-hover', '#2b2b2b');
        root.style.setProperty('--color-letter', '#f4f5ed');
        root.style.setProperty('--primary-color', '#4a4a4a');
 
      }
    })
  }

}
