import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { IContacto } from 'src/interface/IContacto';
import { timer } from 'rxjs';
import {tap, mergeAll, mergeMap} from 'rxjs/operators';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  @Input("item")
  item : IContacto;

  @Output()
  clickRequest = new EventEmitter<IContacto>();

  itemClass : string[];


  constructor() { }

  ngOnInit(): void {
    this.itemClass = [
      "item"
    ]
  }

  onClick(item: IContacto): void{

    timer(100).pipe(
      tap( ()=> this.itemClass.push("item--hover"))
    ).pipe(
      mergeMap( ()=> timer(500))
    ).subscribe(()=> {
      this.clickRequest.emit(item);
    })
   
  }

}
