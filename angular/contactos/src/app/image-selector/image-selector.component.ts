import { Component, OnInit, ViewChild, ElementRef, Input, forwardRef } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-image-selector',
  templateUrl: './image-selector.component.html',
  styleUrls: ['./image-selector.component.scss'],
  providers: [
    {
      multi: true,
      useExisting : forwardRef(() => ImageSelectorComponent),
      provide : NG_VALUE_ACCESSOR
    }
  ]
})
export class ImageSelectorComponent implements OnInit, ControlValueAccessor {

  constructor() { }
  
  image$ : BehaviorSubject<string> = null;

  
  @ViewChild('input', {static: false})
  inputNombre : ElementRef<any>;

  @Input()
  id : String;

  @Input()
  disabled : boolean;


  _classImage : string;


  @Input()
  set classImage(classImage : string){
    this._classImage = classImage;
    this.imagePreviewClass = [ classImage];
  }

  get classImage(): string{
    return this._classImage;
  }



  imagePreviewClass: string[] = [];

  onChange = (__: any) => {};

  onTouch = () => {};

 
  ngOnInit(): void {
  }


  readFile(file : any): Observable<string | ArrayBuffer>{
    return new Observable<string | ArrayBuffer>((subscriber) => {
        const fileReader = new FileReader();

        fileReader.onload = ()=> {
          subscriber.next(fileReader.result);
        };

        fileReader.readAsDataURL(file);
    })
  }


  OnChangeImage(){
    const files : FileList[] = this.inputNombre.nativeElement.files;
    let file : FileList;
 
    if (files.length > 0)
    {
      file = files[0];

      if (this.image$ === null)
      {
        this.image$ = new BehaviorSubject('');
      }

      this.readFile(file).subscribe(this.image$);
      this.onChange(file)
    }
  }

  writeValue(obj: any): void {
      if (obj !== null)
      {
        if (this.image$ === null)
        {
          this.image$ = new BehaviorSubject(obj);
        }

        this.image$.next(obj);
      }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
