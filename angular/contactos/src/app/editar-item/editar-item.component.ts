import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IContacto } from 'src/interface/IContacto';
import { slideAnimation } from '../animation';

@Component({
  selector: 'app-editar-item',
  templateUrl: './editar-item.component.html',
  styleUrls: ['./editar-item.component.scss'],
  animations : [slideAnimation],
  host: { '[@slideAnimation]': '' }
})
export class EditarItemComponent implements OnInit {
  formulario : FormGroup;

  current : IContacto = null;

  constructor(private formBuilder : FormBuilder) {

   }

   buildFormario(){
     this.formulario = this.formBuilder.group({
        avatar : [this.current ? this.current.photo : ''],
        nombre : [this.current ? this.current.nombre : '', Validators.required],
        apellidoPaterno : [this.current ? this.current.apellidoPaterno : '', Validators.required],
        apellidoMaterno : [this.current ? this.current.apellidoMaterno : '', Validators.required],
        salario : [this.current ? this.current.salario : '', Validators.required],
        edad : [this.current ? this.current.edad : '', Validators.required],
        telefono : [this.current ? this.current.tel : '', Validators.required],
        email : [this.current ? this.current.email : '', Validators.compose( [
          Validators.required,
          Validators.email
        ])],
        rfc : [this.current ? this.current.rfc : '' , Validators.compose([
          Validators.required,
        ])]
     })
   }

   submit(){
     console.log(this.formulario.value);
   }

  ngOnInit(): void {
    this.current = history.state;
    this.buildFormario();
  }

}
