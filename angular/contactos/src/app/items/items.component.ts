import { Component, OnInit } from '@angular/core';
import { IContacto } from 'src/interface/IContacto';
import { Router } from '@angular/router';
import {slideAnimation} from 'src/app/animation/index';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss'],
  animations : [slideAnimation],
  host: { '[@slideAnimation]': '' }
  
})
export class ItemsComponent implements OnInit {

  constructor(private router: Router) { }

  public items : IContacto[] = [
    {id: 1, nombre: "Juanito", apellidoMaterno: "Sanchex", apellidoPaterno:"Ramiro", edad: 12, email:"shomares@gmail.com",photo:"https://images.unsplash.com/photo-1586297098710-0382a496c814?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80", salario: 122, tel: "12", rfc: "asdasdasd" },
    {id: 1, nombre: "Juanito", apellidoMaterno: "Sanchex", apellidoPaterno:"Ramiro", edad: 12, email:"shomares@gmail.com",photo:"https://images.unsplash.com/photo-1558898479-33c0057a5d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80", salario: 122, tel: "12" , rfc: "asdasdasd"},
    {id: 1, nombre: "Juanito", apellidoMaterno: "Sanchex", apellidoPaterno:"Ramiro", edad: 12, email:"shomares@gmail.com",photo:"https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80", salario: 122, tel: "12" , rfc: "asdasdasd"} ,
    {id: 1, nombre: "Juanito", apellidoMaterno: "Sanchex", apellidoPaterno:"Ramiro", edad: 12, email:"shomares@gmail.com",photo:"https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80", salario: 122, tel: "12" , rfc: "asdasdasd"},
    {id: 1, nombre: "Juanito", apellidoMaterno: "Sanchex", apellidoPaterno:"Ramiro", edad: 12, email:"shomares@gmail.com",photo:"https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80", salario: 122, tel: "12" , rfc: "asdasdasd"}
  ];

  ngOnInit(): void {
  }

  onClick(item : IContacto){
    this.router.navigate(['/item'], {
      state : {...item}
    })
  }

}
