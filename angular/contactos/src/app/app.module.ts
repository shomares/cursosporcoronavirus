import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemComponent } from './item/item.component';
import { ItemsComponent } from './items/items.component';
import { EditarItemComponent } from './editar-item/editar-item.component';
import {ReactiveFormsModule} from '@angular/forms';
import { InputValidatorComponent } from './input-validator/input-validator.component';
import { ImageSelectorComponent } from './image-selector/image-selector.component';
import { ModeSelectorComponent } from './mode-selector/mode-selector.component';
import { StoreModule } from '@ngrx/store';
import modeReducer from 'src/app/reducers/mode.reducer';

@NgModule({
  declarations: [
    AppComponent,
    ItemComponent,
    ItemsComponent,
    EditarItemComponent,
    InputValidatorComponent,
    ImageSelectorComponent,
    ModeSelectorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    StoreModule.forRoot({
      mode: modeReducer
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
