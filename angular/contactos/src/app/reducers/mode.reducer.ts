import {createReducer, on} from '@ngrx/store';
import { IMode } from 'src/interface/IMode';
import {changeMode} from 'src/app/actions/mode.actions';

const  initialData : IMode = {
    currentMode : "Modo Lighten",
    isWhite : true
};


const reducer =  createReducer(
    initialData,
    on(changeMode, state => 
        ({
            currentMode : state.isWhite ? "Modo Obscuro" : "Modo Lighten",
            isWhite : !state.isWhite
        }))
    )


export default (state : IMode, action)=> reducer(state, action);
