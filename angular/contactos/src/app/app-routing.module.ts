import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ItemsComponent} from './items/items.component';
import {EditarItemComponent} from './editar-item/editar-item.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';



const routes: Routes = [
  {
    path : "items",
    component : ItemsComponent,
  },
  {
    path : "item",
    component: EditarItemComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
