import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-input-validator',
  templateUrl: './input-validator.component.html',
  styleUrls: ['./input-validator.component.scss'],
  providers: [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(()=> InputValidatorComponent),
      multi: true
    }
  ]
})
export class InputValidatorComponent implements OnInit, ControlValueAccessor {
  @Input()
  label : String;

  @Input()
  type: String;

  @Input()
  disabled : Boolean = false;

  @Input()
  id: String;

  value : String = "";

  onChange = (__:any)=> {};
  onTouch = () => {};

  constructor() { }

  onInput(value : String){

    if (this.value !== value)
    {
      this.value = value;
      this.onTouch();
      this.onChange(this.value);
    }
    
  }

  writeValue(obj: any): void {
     this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }


  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  ngOnInit(): void {
  }

}
