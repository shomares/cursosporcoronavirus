import {createAction} from '@ngrx/store';

export const changeMode = createAction('[Mode Component] changeMode');