// import the required animation functions from the angular animations module
import { trigger, animate, transition, style } from '@angular/animations';

export const slideAnimation  = 
trigger('slideAnimation', [

    // route 'enter' transition
    transition(':enter', [

        // css styles at start of transition
        style({ opacity: 0 }),

        // animation and styles at end of transition
        animate('2s', style({ opacity: 1 }))
    ]),
]);

