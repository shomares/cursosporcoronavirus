export interface IContacto {
    id : number;
    photo : string;
    nombre : string;
    apellidoPaterno : string;
    apellidoMaterno : string;
    salario : number;
    edad : number;
    tel : string;
    email : string;
    rfc: string ;
}