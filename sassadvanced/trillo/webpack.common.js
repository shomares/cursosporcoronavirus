const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest')

module.exports = {
    entry: './src/app.ts',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.bundle.js'
    },
    plugins: [
    new HtmlWebpackPlugin(
        {
            template: './src/index.html'
        }
    ),

    new WebpackPwaManifest({
        name: 'Trillo',
        short_name: 'Trillo',
        description: 'Project test avanced sass',
        background_color: '#ffffff',
        crossorigin: 'anonymous', //can be null, use-credentials or anonymous
        icons: [
          {
            src: path.resolve('src/img/favicon.png'),
            size: '1024x1024' // you can also use the specifications pattern
          }
         
        ]
      }),
    new MiniCSSExtractPlugin(),
    new CopyPlugin({
        patterns: [
            { from: './src/img', to: path.join(__dirname, 'dist/img') },
            { from: './src/img/SVG', to: path.join(__dirname, 'dist/img/SVG') },
        ],
    }),
    ],
    module: {
        rules: [
            { 
                test: /\.scss$/, 
                loader: [
                  MiniCSSExtractPlugin.loader,
                  "css-loader",
                  'sass-loader'
                ]
            },
            { 
                test: /\.css$/, 
                loader: [
                  MiniCSSExtractPlugin.loader,
                  "css-loader",
                ]
            },
            {
                test: /\.(png|jpe?g|gif|svg|ttf|woff|eot)$/i,
                use: [
                    {
                        loader: 'file-loader',
                    },
                ]
            },
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
              }
        ]
    },
    resolve: {
        extensions: ['.ts', '.js', '.json']
    }
};