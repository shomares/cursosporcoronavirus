import './sass/main.scss';
import {fromEvent} from 'rxjs'
import {filter, tap} from 'rxjs/operators'
import {getObservable} from './scrollanimation';

const divProfile = document.querySelector('#profile-menu');
const scroll :HTMLElement = document.querySelector('#scroll');

const click$ = fromEvent<MouseEvent>(document, 'click');
const mouseMove$ = fromEvent<MouseEvent>(divProfile, 'mousemove');


mouseMove$.pipe(
    filter<MouseEvent>( s=>  
        window.location.hash !== '#menu'
    )
).subscribe(()=> {
    window.location.hash = '#menu'
});

click$.pipe(
    tap( ()=> console.log(window.location.hash)),
    filter<MouseEvent>( s=>  
            window.location.hash === '#menu'
    )
).subscribe(()=> window.location.hash = '');

getObservable().subscribe( s=> scroll.style.width = s);