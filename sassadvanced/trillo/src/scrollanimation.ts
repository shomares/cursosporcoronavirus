import { fromEvent, Observable } from "rxjs";
import { pluck, map } from "rxjs/operators";

interface ScrollData{
        scrollTop : number; 
        clientHeight: number;
        scrollHeight : number;
}

const calcular =  (scroll : ScrollData ) : number =>{
    return scroll.scrollTop / (scroll.scrollHeight - scroll.clientHeight);
}

export const getObservable = () : Observable<string> => {
    const scroll$ = fromEvent(window, 'scroll').pipe(
        pluck<Event, any>('target', 'documentElement'),
        map<any, ScrollData>(s=>{
            return {
                clientHeight : s.clientHeight,
                scrollHeight : s.scrollHeight,
                scrollTop : s.scrollTop
            }
        }),
        map( calcular),
        map(s=> (s * 100) + 10),
        map(s=> `${s}%`)
    )

    return scroll$;
}

