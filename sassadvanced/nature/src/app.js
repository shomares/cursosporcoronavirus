import './css/icon-font.css'
import './sass/main.scss';
import { Observable, fromEvent } from 'rxjs';
import { debounceTime, filter, map, pluck } from 'rxjs/operators'

const registerWorkLoader = () => {
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
            navigator.serviceWorker.register('/service-worker.js').then(registration => {
                console.log('SW registered: ', registration);
            }).catch(registrationError => {
                console.log('SW registration failed: ', registrationError);
            });
        });
    }
}

const calcular =  ({scrollTop, clientHeight, scrollHeight}) =>{
    return scrollTop / (scrollHeight - clientHeight);
}

const scrollbar = ()=> {
    const progressBar = document.querySelector('#scroll');
    
    const scroll$ = fromEvent(window, 'scroll').pipe(
        pluck('target', 'documentElement'),
        map(s=>{
            return {
                clientHeight : s.clientHeight,
                scrollHeight : s.scrollHeight,
                scrollTop : s.scrollTop
            }
        }),
        map( calcular),
        map(s=> s * 100),
        map(s=> `${s}%`)
    )

    scroll$.subscribe(s => progressBar.style.width = s);
}

const setup = () => {
    const checkbox = document.querySelector('#nav_check');
    const getObservable = () =>
        new Observable(obs => {
            window.onhashchange = () => {
                obs.next(true);
            };
        });


    const clicks$ = getObservable();

    clicks$.pipe(
        filter(val => checkbox.checked && val),
        debounceTime(100)
    )
        .subscribe(val => {
            checkbox.checked = false;
        });
};

(()=> {
    registerWorkLoader();
    setup();
    scrollbar();
})();

