const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest')

module.exports = {
    entry: './src/app.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.bundle.js'
    },
    plugins: [
    new HtmlWebpackPlugin(
        {
            template: './src/index.html'
        }
    ),

    new WebpackPwaManifest({
        name: 'Nature',
        short_name: 'Nature',
        description: 'Project test avanced sass',
        background_color: '#ffffff',
        crossorigin: 'anonymous', //can be null, use-credentials or anonymous
        icons: [
          {
            src: path.resolve('src/img/logo-green-1x.png'),
            size: '1024x1024' // you can also use the specifications pattern
          }
         
        ]
      }),
    new MiniCSSExtractPlugin(),
    new CopyPlugin({
        patterns: [
            { from: './src/img', to: path.join(__dirname, 'dist/img') },
            { from: './src/css/fonts', to: path.join(__dirname, 'dist/css/fonts') },
        ],
    }),
    new WorkboxPlugin.GenerateSW({
        clientsClaim: true,
        skipWaiting: true,
        }),
    ],
    module: {
        rules: [
            { 
                test: /\.scss$/, 
                loader: [
                  MiniCSSExtractPlugin.loader,
                  "css-loader",
                  'sass-loader'
                ]
            },
            { 
                test: /\.css$/, 
                loader: [
                  MiniCSSExtractPlugin.loader,
                  "css-loader",
                ]
            },
            {
                test: /\.(png|jpe?g|gif|svg|ttf|woff|eot)$/i,
                use: [
                    {
                        loader: 'file-loader',
                    },
                ]
            }
            
        ]
    }
};