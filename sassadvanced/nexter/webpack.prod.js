const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');


module.exports = merge(common,
    {
        mode: 'production',
        optimization: {
            minimizer: [new OptimizeCSSAssetsPlugin({})],
        },
        plugins : [
            new WorkboxPlugin.GenerateSW({
                clientsClaim: true,
                skipWaiting: true,
                }),
        ]
    }
);
