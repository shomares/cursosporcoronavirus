
import React from 'react';
import ToastScreen from './src/Screens/Toast';
import VibrateScreen from './src/Screens/Vibrate';
import Temperature from './src/Screens/Temperature'
import Home from './src/Screens/Home'
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';


const Drawer = createDrawerNavigator();

const Navigation = () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="home" >
        <Drawer.Screen component={Home} name="home" options={{
          title: "Home"
        }} />
        <Drawer.Screen component={ToastScreen} name="toast" options={{
          title: "Toast"
        }} />
        <Drawer.Screen component={VibrateScreen} name="vibrate" options={{
          title: "Vibration"
        }} />

        <Drawer.Screen component={Temperature} name="temperature" options={{
          title: "Temperature"
        }} />

      </Drawer.Navigator>
    </NavigationContainer>
  )
}


export default Navigation;


