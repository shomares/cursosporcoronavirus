package com.modules.Temperature;

import android.content.Context;

import com.modules.Temperature.impl.TemparatureCpuService;
import com.modules.Temperature.impl.TemperatureCpuServiceNoRoot;
import com.scottyab.rootbeer.RootBeer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class ProviderTemperatureService {

    private Map<String, Class<?>> map;

    private  static  ProviderTemperatureService instance;

    public  static  ProviderTemperatureService getInstance(){
        if (instance == null)
        {
            instance = new ProviderTemperatureService();
        }

        return instance;
    }
    private ProviderTemperatureService()
    {
        this.map = new HashMap<>();
        this.map.put("rooted", TemparatureCpuService.class);
        this.map.put("aleatory", TemperatureCpuServiceNoRoot.class);
    }

    public ITemperatureService getService(Context context) throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        RootBeer rootBeer = new RootBeer(context);

        Class<?> type = rootBeer.isRooted() ? this.map.get("rooted"): this.map.get("aleatory");
        Constructor<?> constructor = type.getConstructor();
        ITemperatureService result = (ITemperatureService)constructor.newInstance();
        return result;
    }
}
