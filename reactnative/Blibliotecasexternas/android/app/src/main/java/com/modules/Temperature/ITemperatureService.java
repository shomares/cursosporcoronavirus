package com.modules.Temperature;

/**
 *
 */
public interface ITemperatureService {
     double getTemperature();
}
