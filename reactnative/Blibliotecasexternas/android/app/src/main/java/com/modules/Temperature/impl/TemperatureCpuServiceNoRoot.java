package com.modules.Temperature.impl;

import com.modules.Temperature.ITemperatureService;

public class TemperatureCpuServiceNoRoot  implements ITemperatureService {
    @Override
    public double getTemperature() {
        return Math.random() * 100 ;
    }
}
