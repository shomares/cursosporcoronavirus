package com.modules.Vibrator;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

/**
 * Module, let you make the phone vibrate
 */
public class VibratorModule extends ReactContextBaseJavaModule {


    /**
     * Create an instance of ToastModule
     * @param contextBaseJavaModule Context of the application
     */
    public VibratorModule(ReactApplicationContext contextBaseJavaModule)
    {
        super(contextBaseJavaModule);
    }


    /**
     * Makes the phone vibrate,
     * @param time How long the vibration will late, it must be in miliseconds
     * @param promise Returns a promise, on js code you have to run this function through async/await method
     */
    @ReactMethod
    public void Vibrate (Integer time, Promise promise)
    {
        Context ctx = this.getReactApplicationContext();
        Vibrator v = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(time, VibrationEffect.DEFAULT_AMPLITUDE));
        }else
        {
            v.vibrate(time);
        }

        promise.resolve(null);
    }

    /**
     * Returns the name of module, its so important, when you use in js code
     * you  have to write import {NativeModule} from 'react-native';
     * NativeModule.vibrator
     * @return A string
     */
    @NonNull
    @Override
    public String getName() {
        return "vibrator";
    }
}
