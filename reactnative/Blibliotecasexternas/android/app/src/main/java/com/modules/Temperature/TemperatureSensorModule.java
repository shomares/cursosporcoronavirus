package com.modules.Temperature;

import android.os.Handler;
import androidx.annotation.NonNull;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.modules.Temperature.impl.TemparatureCpuService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.internal.operators.flowable.FlowableInterval;


public class TemperatureSensorModule extends ReactContextBaseJavaModule {

    private  static  final  String EVENT_NAME = "CPU_SENSOR";

    private Disposable subscription;
    /**
     * Create an instance of ToastModule
     * @param contextBaseJavaModule Context of the application
     */
    public TemperatureSensorModule(ReactApplicationContext contextBaseJavaModule)
    {
        super(contextBaseJavaModule);
    }

    @ReactMethod
    public void Dispose(Promise promise)
    {
        if (this.subscription != null)
        {
            this.subscription.dispose();
        }

        promise.resolve(true);
    }

    /**
     * Starts to get the temperature of cpu
     * @param interval The interval in milleseconds
     */
    @ReactMethod
    public void OnSense(final Integer interval, Promise promise)
    {
        ReactContext context = this.getReactApplicationContext();
        WritableMap result = Arguments.createMap();

        try
        {
            final ITemperatureService service =  ProviderTemperatureService.getInstance().getService(context);
            Flowable<Long>  timer$ = Flowable.interval(0, interval, TimeUnit.MILLISECONDS);

            if (this.subscription != null)
            {
                this.subscription.dispose();
            }

            this.subscription = timer$.map( val-> {
                return service.getTemperature();
            }).map(val -> {
                WritableMap params = Arguments.createMap();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
                String currentDateandTime = sdf.format(new Date());
                params.putDouble("current", val);
                params.putString("time", currentDateandTime);
                return params;
            }).subscribe(onNext ->{
                context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                        .emit(EVENT_NAME, onNext );
            });

            result.putString("Evt" , "Success");
            result.putInt("Code", 200);
            promise.resolve(result);
        }catch (Exception ex)
        {
            promise.reject(ex);
        }
      }

    /**
     * Return the name of this module
     * @return A string
     */
    @NonNull
    @Override
    public String getName() {
        return "temperature";
    }
}
