package com.modules.Temperature.impl;

import com.facebook.react.bridge.ReactContext;
import com.modules.Temperature.ITemperatureService;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TemparatureCpuService implements ITemperatureService {

    public double getTemperature(){
        Process p = null;
        try {
            p = Runtime.getRuntime().exec("cat sys/class/thermal/thermal_zone0/temp");
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = reader.readLine();
            double temp = Double.parseDouble(line) / 1000.0f;

            return temp;

        } catch (Exception e) {
            e.printStackTrace();
            return 0.0f;
        }
    }


}
