package com.modules.Vibrator;

import androidx.annotation.NonNull;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VibratorPackage   implements ReactPackage {

    /**
     * Overwrite createNativeModules, registers the vibrators module.
     * @param reactContext Context of the application
     * @return A list of Native Module
     */
    @NonNull
    @Override
    public List<NativeModule> createNativeModules(@NonNull ReactApplicationContext reactContext) {
        List<NativeModule> module = new ArrayList<>();
        module.add(new VibratorModule(reactContext));
        return module;
    }

    /**
     * Overwrite createViewManagers, just for UI modules
     * @param reactContext Context of the application
     * @return A empty list
     */
    @NonNull
    @Override
    public List<ViewManager> createViewManagers(@NonNull ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }
}
