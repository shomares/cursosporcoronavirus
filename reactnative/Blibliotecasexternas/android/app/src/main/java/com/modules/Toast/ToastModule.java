package com.modules.Toast;

import android.widget.Toast;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

/**
 * Module of toast
 */
public class ToastModule extends ReactContextBaseJavaModule {

    /**
     * Create an instance of ToastModule
     * @param contextBaseJavaModule Context of the application
     */
    public ToastModule(ReactApplicationContext contextBaseJavaModule)
    {
        super(contextBaseJavaModule);
    }


    /**
     * Shows a Toast's message, it is available in js code
     * @param message Message will be showed
     */
    @ReactMethod
    public void Show(String message)
    {
        Toast.makeText(this.getReactApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    /**
     * Return the name of this module
     * @return A string
     */
    @NonNull
    @Override
    public String getName() {
        return "ToastModule";
    }
}
