package com.modules.Toast;


import androidx.annotation.NonNull;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Package of toast's module
 */
public class ToastPackage implements ReactPackage {


    /**
     * Overwrite the method createNativeModules, it registers the Toasts module
     * @param reactContext Applications context
     * @return A list of all modules
     */
    @NonNull
    @Override
    public List<NativeModule> createNativeModules(@NonNull ReactApplicationContext reactContext) {
        List<NativeModule> module = new ArrayList<>();
        module.add(new ToastModule(reactContext));
        return module;
    }

    /**
     * Overrite the method createViewManagers,I dont know what it means
     * @param reactContext Applications context
     * @return A list of ViewManager, in this case an empty list
     */
    @NonNull
    @Override
    public List<ViewManager> createViewManagers(@NonNull ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }
}
