
import React from 'react';
import Toast from '../external/Toast';

import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

export default () => {
  const toast = Toast();

  return (
    <View style={{
      flex: 1,
    }}>
        <TouchableOpacity style={{
          borderRadius: 40,
          width: 80,
          height: 80,
          backgroundColor: 'red',
          alignItems: "center",
        }}
          onPress={() => toast.show('Test')}
        >
          <Text>Show </Text>
        </TouchableOpacity>
    </View>

  )
}