
import React, { useState } from 'react';
import Vibrate from '../external/Vibrate';

import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
} from 'react-native';



export default () => {
  const vibrate = Vibrate();

  const [time, setTime] = useState('1000');

  return (
    <View style={{
      flex: 1,
    }}>

      <View>

        <View>
          <Text>
            Tiempo:
          </Text>
          <TextInput
            value={time}
            onChangeText={(text) => setTime(text)}
            numberOfLines = {1}
          />
        </View>


        <TouchableOpacity style={{
          borderRadius: 40,
          width: 80,
          height: 80,
          backgroundColor: 'red',
          alignItems: "center",
        }}
          onPress={() => vibrate.vibrate(parseInt(time))}
        >
          <Text>Show </Text>
        </TouchableOpacity>
      </View>
    </View>

  )
}