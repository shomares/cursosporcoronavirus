
import React ,{useEffect, useState} from 'react';
import Temperature from '../external/Temperature';


import {
  View,
  Text,
  Animated,
  Dimensions
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default () => {
  const temp = Temperature();
  let subscription = null
  const SCREEN_HEIGTH = Dimensions.get("screen").height;
  const size = SCREEN_HEIGTH - 20;
 
  const [currentHeigth, setCurrentHeigth] = useState(1);
  
  const [temperature, setTemperature] = useState({current : null, time: null});

  const position = new Animated.Value(currentHeigth);
  const interpolate = position.interpolate({
    inputRange : [0 , 100],
    outputRange : [50, 0]
  });

  useEffect(()=> {
        temp.onSense(3000).then($temperatura => {
          subscription =  $temperatura.subscribe((val)=> {
            setTemperature(val);
          });
        });
      
        return ()=>{
          console.log('Dispose');
          temp.dispose();
          subscription = null;
        };
  }, [])

  useEffect (()=> {
    let {current} = temperature;
    if(current !== null)
    {
      console.log("Interpolate:", interpolate);
      Animated.timing(position, {
        toValue : current,
        duration : 2000,
        useNativeDriver : false
      }).start(()=> {
        setCurrentHeigth(current);
      });
    }

  }, [temperature.current])

  return (
    <View style={{
      flex: 1,
    }}>
          <Text>{temperature.current}</Text>
          <View style= {{
            flex : 1,
            alignItems: "center"
          }}>
              <Animated.View style= {{
                width : 10,
                    transform: [
                  {  scaleY: interpolate } // this would be the result of the animation code below and is just a number.
                ]
              }}> 

                <TouchableOpacity style={{
                  backgroundColor : "red",
                }}>

                    <Text></Text>

                </TouchableOpacity>

              </Animated.View>
          </View>

    </View>

  )
}