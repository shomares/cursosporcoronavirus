import {NativeModules} from 'react-native';

const show = (message)=>  {
    //console.log(NativeModules.Toast.message)
    NativeModules.ToastModule.Show(message);
};

export default ()=> ({
    show
});
