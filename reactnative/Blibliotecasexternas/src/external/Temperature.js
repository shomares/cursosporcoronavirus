import { NativeEventEmitter, NativeModules } from 'react-native';
import { Observable, of } from 'rxjs';


const dispose = async ()=> {
    const {temperature}  = NativeModules;
    await temperature.Dispose();
}

const onSense = async (interval = 1000)=>  {
    const {temperature}  = NativeModules;
    const eventEmitter = new NativeEventEmitter(temperature);

    try{
        await temperature.OnSense(interval);
        return new Observable((subscriber)=> {
            eventEmitter.addListener('CPU_SENSOR', val=> 
                subscriber.next(val)
             );
            
        });
    
    }catch(ex)
    {
        return of(ex);
    }
};

export default ()=> ({
    onSense,
    dispose
});
