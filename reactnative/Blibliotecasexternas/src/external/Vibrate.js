import {NativeModules} from 'react-native';

const vibrate = async (time = 1000)=> {
    const {vibrator} = NativeModules;
    await vibrator.Vibrate(time);
};

export default () => ({
    vibrate
});