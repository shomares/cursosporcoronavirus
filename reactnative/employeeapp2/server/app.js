const dotenv = require('dotenv');
dotenv.config();

const express = require('express');

const AWS = require('aws-sdk');

const app =  express();

const bodyParser = require('body-parser');

const mongoose = require('mongoose');
require('./Employee');


app.use(bodyParser.json());

const Employee = mongoose.model('employee');

const { from, of, zip} = require('rxjs');
const {  mergeMap, map, reduce} = require('rxjs/operators');


const mongoUri = "mongodb://mongoadmin:secret@localhost"




mongoose.connect(mongoUri , {
    useNewUrlParser : true,
    useUnifiedTopology : true
});

mongoose.connection.on('connected', ()=> {
    console.log('connect to mongo');
})

mongoose.connection.on('error', (error)=> {
    console.error('error', error);
})

app.get('/', (req, res)=>{
    res.send('health');
});


const getUrls = (values= [])=>{
    return from(values)
    .pipe(
        mergeMap (  val => zip(  of(val), from(getURL(val.picture)))),
        map ( ([val, picture]) => ({
            ...val,
            picture
        })),
        reduce( (acc, val) => [...acc, val], [])
    )
};

const getURL = async (name) => {
    var s3 = new AWS.S3({accessKeyId:process.env.accessKeyId, secretAccessKey:process.env.secretAccessKey, region:'us-west-2'});
    
    var params = {
        Bucket: 'websitecourseeneas', 
        Key: 'images/'+ name
    };
    
    return await s3.getSignedUrl('getObject', params);
    
};



app.post('/employee', async (req, res)=>{

    const employee = new Employee({
        name : req.body.name,
        email : req.body.email,
        phone : req.body.phone,
        picture : req.body.picture,
        salary : req.body.salary,
        position : req.body.position
    });

    try{
        let data = await employee.save();
        console.log(data);
        res.send(data);
    }catch(e)
    {
        console.log(e);
        res.send('error');
    }
   
});

app.delete('/employee/:id', async (req, res)=>{
    console.log(req.params.id);
    let ret = await Employee.findByIdAndRemove(req.params.id);
    res.send(ret);

});

app.get('/employee', async(req, res)=>{
    let data = await Employee.find({}).lean();

    getUrls(data).subscribe(
        val =>  res.send(val)
    );
   
   
});




app.put('/employee/:id', async (req, res)=>{
    try{
    
        let data = await Employee.findByIdAndUpdate(req.params.id, {
            name : req.body.name,
            email : req.body.email,
            phone : req.body.phone,
            picture : req.body.picture,
            salary : req.body.salary,
            position : req.body.position
        });
        res.send(data);
    }catch(e)
    {
        console.error(e);
        res.send('error');
    }
});

app.post('/signedPost', async(req, res)=>{
    var s3 = new AWS.S3({accessKeyId:process.env.accessKeyId, secretAccessKey:process.env.secretAccessKey, region:'us-west-2'});
    let {name, type, uri } = req.body;
    
    var params = {
        Bucket: 'websitecourseeneas', 
        Key: 'images/'+ name,
        ContentType: 'image/jpeg'
    };
    
    let presignedUrl = await s3.getSignedUrl('putObject', params);
    console.log('presignedUrl', presignedUrl);
    res.send({presignedUrl});

});




app.listen(3000, ()=>{

    console.log('server running');
} );