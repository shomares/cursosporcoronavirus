export const initalState = {
    data : [],
    loading : true
}


export const reducer = (state, action)=>{
    if (action.type === "ADD_DATA")
    {
        return {...state, data : action.payload};
    }

    if (action.type === "SET_LOAD")
    {
        return {...state, loading : action.payload};
    }
    
    return state;
};
