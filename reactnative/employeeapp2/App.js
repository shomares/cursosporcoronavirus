import React, {createContext, useReducer} from 'react';
import { StyleSheet,View } from 'react-native';
import Constants from 'expo-constants';
import Home from './screens/Home';
import CreateEmployee from './screens/CreateEmployee';
import Profile from './screens/Profile';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'

import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {reducer, initalState} from './reducers/reducer';

//const store = createStore(reducer);


export const myContext = createContext();

const Stack = createStackNavigator();



const myoptions= {
  title : "My sweet home",
              headerTintColor : "white",
              headerStyle: {
                backgroundColor : "#006aff"
              }
}


function App() {
  return (
    <View style={styles.container}>
      <Stack.Navigator>
        <Stack.Screen 
            name="Home"
            component={Home}
            options =  {myoptions}
         />
        <Stack.Screen 
          name="Profile" 
          component={Profile} 
          options =  { {...myoptions , title : "Profile"}}

        />
        <Stack.Screen 
          name="Create" 
          component={CreateEmployee}
          options =  { {...myoptions , title : "Create Employee"}}

           />
      </Stack.Navigator>
    </View>
  );
}

export default () => {
  const [state, dispatch] = useReducer(reducer, initalState);
  return (
    <myContext.Provider value={
      {
        state,
        dispatch
      }
    }>
        <NavigationContainer>
          <App />
        </NavigationContainer>
    </myContext.Provider>
  
  );

};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e0e0e0',
    
  },
});
