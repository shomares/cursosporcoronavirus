const API = 'http://192.168.0.11:3000';

const postSignedFile = async (file) => {
    let {name, type} = file;
    let result = await fetch(`${API}/signedPost`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({name, type})
    })

    return await result.json();
}

export const  AwsService =  ()=>({
    postSignedFile
})

