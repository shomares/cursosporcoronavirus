const API = 'http://192.168.0.11:3000';
const save = async (data)=> {
    let result = await fetch(`${API}/employee`, {
        body : JSON.stringify(data),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method : 'post'
    } );

    return await result.json();
};

const get = async ()=> {
    let result = await fetch(`${API}/employee`, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method : 'get'
    } );

    return await result.json();
};

const deleteEmpl = async(id) => {
    let result = await fetch(`${API}/employee/${id}`, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method : 'delete'
    } );

    return await result.json();
}


const update = async(id, data) => {
    let result = await fetch(`${API}/employee/${id}`, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body : JSON.stringify(data),
        method : 'put'
    } );

    return await result.json();
}

const employeeService = ()=> ({save, get, deleteEmpl, update});

export default employeeService;