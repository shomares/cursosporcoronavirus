import React, { useState } from 'react';
import { StyleSheet, View, Modal, Alert, KeyboardAvoidingView } from 'react-native';
import { TextInput, Button } from 'react-native-paper';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import {AwsService} from '../services/awsservice';
import EmployeeService from '../services/employee.service';

const CreateEmployee = ({navigation, route}) => {

    const getDetails = (type)=> {
        if (route.params)
        {
            return route.params[type];
        }
        else
        {
            return "";
        }
    };
    
    const [name, setName] = useState(getDetails('name'));
    const [phone, setPhone] = useState(getDetails('phone'));
    const [email, setEmail] = useState(getDetails('email'));
    const [position, setPosition] = useState(getDetails('position'));
    
    const [salary, setSalary] = useState(getDetails('salary'));
    const [picture, setPicture] = useState(getDetails('picture'));
    const [modal, setModal] = useState(false);
    const [enabledshift,setEnabledShift] = useState(false);



    const updateDetails = async()=> {
        let service = EmployeeService();
        try{
            let result = await service.update(route.params._id, {
                name,
                phone,
                email,
                position,
                salary,
                picture
            });

            console.log(result);
            Alert.alert(`${result.name} is updated sucessfully`);
            navigation.navigate('Home');
        }catch(err){
            Alert.alert('Something went wrong');
        }
    }


    const submitData = async ()=> {
        let service = EmployeeService();
        let result = await service.save({
            name,
            phone,
            email,
            position,
            salary,
            picture
        });

        console.log(result);
        Alert.alert(`${result.name} is saved sucessfully`);
        navigation.navigate('Home');
    }

    const pickFromGalery = async ()=> {
        const {granted} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if(granted)
        {
            let data = await ImagePicker.launchImageLibraryAsync(
                {
                    mediaTypes:  ImagePicker.MediaTypeOptions.Images,
                    allowsEditing : true,
                    aspect : [1,1],
                    quality : 0.5
                }
            );

            await uploadFile(data);

        }else{
            Alert.alert("You need to give us permission to work");
        }
    }



    
    const pickFromCamera = async ()=> {
        const {granted} = await Permissions.askAsync(Permissions.CAMERA);
        if(granted)
        {
            let data = await ImagePicker.launchCameraAsync(
                {
                    mediaTypes:  ImagePicker.MediaTypeOptions.Images,
                    allowsEditing : true,
                    aspect : [1,1],
                    quality : 0.5
                }
            );

            await uploadFile(data);
        }else{
            Alert.alert("You need to give us permission to work");
        }
    }

    const uploadFile  = async (data) => {
            if (!data.cancelled)
            {
                let names  = data.uri.split('/');
                let name = names[names.length - 1 ];
                let type =  name.split('.')[1];

                let file = {
                    name,
                    type,
                    uri : data.uri
                }

                await handleUpload(file);
                setPicture(name);
                setModal(false);
            }
        

    }

    const handleUpload = async (file)=>{
        let s3Service = AwsService();

        let {presignedUrl} = await s3Service.postSignedFile(file);
        let {uri, type, name} = file;

        const xhr = new XMLHttpRequest()
        xhr.open('PUT', presignedUrl)
        xhr.addEventListener('load', (data)=>{
                console.log(data);
        });
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    console.log('Image successfully uploaded to S3')
                } else {
                    console.log(xhr.sta)
                    console.log(xhr.status);
                    //throw 'Error while sending the image to S3';
                }
            }
        }
        
        xhr.setRequestHeader('Content-Type', 'image/jpeg')
        xhr.send({ uri, type, name })
    }


    return (
        <KeyboardAvoidingView behavior="position"  style={styles.root} enabled={enabledshift}>
       
        <View >
            <TextInput
                style={styles.inputStyle}
                label='Name'
                value={name}
                onFocus = {()=> setEnabledShift(false)}
                mode="outlined"
                theme={theme}
                onChangeText={text => setName(text)}
            />
            <TextInput
                style={styles.inputStyle}
                label='Email'
                onFocus = {()=> setEnabledShift(false)}
           
                value={email}
                mode="outlined"
                theme={theme}
                onChangeText={text => setEmail(text)}
            />

            <TextInput
                style={styles.inputStyle}
                label='Position'
                onFocus = {()=>setEnabledShift(false)}
           
                value={position}
                mode="outlined"
                theme={theme}
                onChangeText={text => setPosition(text)}
            />  

            <TextInput
                style={styles.inputStyle}
                label='Phone'
                value={phone}
                mode="outlined"
                theme={theme}
                onFocus = { ()=> setEnabledShift(true)}
                keyboardType="number-pad"
                onChangeText={text => setPhone(text)}
            />

            <TextInput
                style={styles.inputStyle}
                label='Salary'
                value={salary}
                mode="outlined"
                theme={theme}
                onFocus = {()=> setEnabledShift(true)}
                keyboardType="number-pad"
                onChangeText={text => setSalary(text)}
            />

            <Button 
                theme={theme} 
                style={styles.inputStyle} 
                icon={picture === "" ? "upload" : "check"} 
                mode="contained" 
                onPress={() => setModal(true)}>
                Upload image
            </Button>

            {!route.params ?
                <Button icon="content-save"
                    theme={theme}
                    style={styles.inputStyle}
                    mode="contained"
                    onPress={() => submitData()}>
                    Save 
                </Button>
                :
                <Button icon="content-save"
                theme={theme}
                style={styles.inputStyle}
                mode="contained"
                onPress={() => updateDetails()}>
                Update details 
            </Button>

            } 

            <Modal
                animationType="slide"
                transparent={true}
                visible={modal}
                onRequestClose={() => {
                    setModal(false)
                }}
            >
                <View style={styles.modalView}>
                    <View style={styles.modalButtonView}>
                        <Button icon="camera"
                            theme={theme}
                            mode="contained"
                            onPress={() => pickFromCamera()}>
                            camera
                        </Button>
                        <Button icon="image-area"
                            theme={theme}
                            mode="contained"
                            onPress={() => pickFromGalery()}>
                            gallery
                        </Button>
                    </View>
                    <Button theme={theme} onPress={() => setModal(false)}>
                        cancel
                    </Button>
                </View>

            </Modal>
        </View>

        </KeyboardAvoidingView>


    );
}

const styles = StyleSheet.create({
    root: {
        flex: 1
    },
    inputStyle: {
        margin: 5
    },
    modalButtonView: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    },

    modalView: {
        position : "absolute",
        bottom: 2,
        width: "100%",
        backgroundColor: "white"
    }
});

const theme = {
    colors: {
        primary: "#006aff"
    }
};


export default CreateEmployee;