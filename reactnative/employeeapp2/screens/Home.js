import React, {useEffect, useState, useContext} from 'react';
import { StyleSheet, Text, View, Image, FlatList, Alert } from 'react-native';
import { Card, FAB } from 'react-native-paper';
import EmployeeService from '../services/employee.service';
import {useSelector, useDispatch} from 'react-redux';

import {myContext} from '../App';

const Home = ({navigation}) => {

    //const [data, setdata] = useState([]);

    //const [loading , setLoading] = useState(true);

    //Like constructor

    /*
    const {data, loading} = useSelector((state)=>{
        return state;
    });
    */

    const {state, dispatch} = useContext(myContext);
    const {data, loading} = state;

    //const dispatch = useDispatch();

    useEffect (()=> {
        fetchData().then(console.log('sucesss!'));
    }, []);


    const fetchData =  async ()=> {
        try{
            let service = EmployeeService();
            let data = await service.get();
            
            /*
            setdata(data);
            setLoading(false);
            */

           dispatch({
               type : "ADD_DATA",
               payload : data
           });

           dispatch({
                type : "SET_LOAD",
                payload: false
           });


        }catch(err){
            Alert.alert('something went wrong')
        } 
       
    };

    const renderList = (item = { _id: null, name: null, position: null }) => {
            return (
                <Card style={styles.mycard} key={item._id} onPress={ ()=>{
                    navigation.navigate("Profile", {item : {...item}})

                }}>
                    <View style={styles.cardView}>
                        <Image
                            style={{ width: 60, height: 60, borderRadius: 30 }}
                            source={{ uri: item.picture }}
                        />
                        <View styles={{ marginLeft: 10 }}>
                            <Text style={styles.text}>{item.name}</Text>
                            <Text style={styles.text}>{item.position}</Text>
                        </View>

                    </View>

                </Card>
            );
        }




    return (
        <View style= {{flex : 1}}>
                <FlatList
                    data={data}
                    renderItem={({ item }) => {
                        return renderList(item);
                    }}
                    keyExtractor={item => item._id}
                    refreshing = {loading}
                    onRefresh = {()=>{fetchData()}}

                />
            <FAB onPress= { ()=> {
                navigation.navigate("Create")
            }}
                style={styles.fab}
                small
                icon="plus"
                theme = {{
                    colors: {
                        accent : "#006aff"
                    }
                }}
               
            />
        </View>



    )
};

const styles = StyleSheet.create({
    mycard: {
        margin: 5,
    },
    cardView: {

        flexDirection: "row",
        padding: 6
    },
    text: {
        fontSize: 20,
        marginLeft: 10
    },
    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
    },
});

export default Home;