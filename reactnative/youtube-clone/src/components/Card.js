import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity } from 'react-native';
import {AntDesign, Ionicons,MaterialIcons} from '@expo/vector-icons';
import Constants from 'expo-constants';
import {useNavigation, useTheme} from '@react-navigation/native';




const Card = ({videoId, title, channel})=> {
    const navigation = useNavigation();
    const {colors} = useTheme();
    const mycolor = colors.iconColor;
  
    return (

        <TouchableOpacity
         onPress = {()=> navigation.navigate("videoPlayer", {
            videoId,
            title
         })}
        >        
            <View style = {{
                marginBottom : 10
            
            }} 
           
            >
                <Image
                    source = {{uri :  `https://i.ytimg.com/vi/${videoId}/hqdefault.jpg`}}
                    style = {{
                        width : "100%",
                        height : 200
                    }}
                />
                <View style={
                    {
                        flexDirection : "row",
                        margin : 5
                    }
                }>
                    <MaterialIcons 
                    name = "account-circle"
                    size = {32}
                    color = {mycolor}
                    />
                    <View
                        style= {{
                            marginLeft : 10
                        }}
                    >
                        <Text 
                            ellipsizeMode = "middle"
                            style= {{
                                fontSize : 20,
                                width: Dimensions.get("screen").width - 50,
                                color : mycolor
                                
                            }}
                            numberOfLines = {2}
                        >
                            {title}
                        </Text>
                        <Text style={{
                            color : mycolor
                        }}>{channel}</Text>
                    </View>
                </View>
            
            </View>
        </TouchableOpacity>

    );


};

export default Card;