import React from 'react';
import { StyleSheet, Text, View, FlatList, ScrollView, Animated } from 'react-native';
import Header from '../../components/Header';
import Card from '../Card';
import { useSelector } from 'react-redux';
import Constant from 'expo-constants'

const LilCard = ({ name }) => {
    return (
        <View
            style={{
                backgroundColor: 'red',
                width: 180,
                height: 50,
                borderRadius: 4,
                marginTop: 10

            }}
        >
            <Text style={{
                textAlign: "center",
                color: "white",
                fontSize: 22,
                marginTop: 5

            }} >{name}</Text>
        </View>)
}

const Explore = () => {

    const cardData = useSelector(state => state.cardData);

    const scrollY = new Animated.Value(0);
    const diffClamp = Animated.diffClamp(scrollY, Constant.statusBarHeight, Constant.statusBarHeight + 45);


    const translateY = diffClamp.interpolate({
        inputRange: [Constant.statusBarHeight, Constant.statusBarHeight + 35],
        outputRange: [Constant.statusBarHeight, Constant.statusBarHeight - 100]
    })

    return (
        <View style={{
            flex: 1,
        }}>
            <Animated.View
                style={{
                    transform: [
                        {
                            translateY
                        }
                    ],
                    elevation: 4,
                    zIndex: 100

                }}>
                <Header />
            </Animated.View>
            <ScrollView>

                <View style={
                    {
                        flexDirection: "row",
                        flexWrap: "wrap",
                        justifyContent: "space-around"

                    }
                } >
                    <LilCard name="Gaming" />
                    <LilCard name="Trending" />
                    <LilCard name="Music" />
                    <LilCard name="News" />
                    <LilCard name="Movies" />
                    <LilCard name="Fashion" />
                </View>

                <Text style={{
                    margin: 8,
                    fontSize: 22,
                    borderBottomWidth: 1
                }}>Trending Videos</Text>


                <FlatList
                    data={cardData}
                    renderItem={({ item }) => {
                        return <Card
                            videoId={item.id.videoId}
                            title={item.snippet.title}
                            channel={item.snippet.channelTitle}
                        />
                    }}
                    keyExtractor={
                        item => item.id.videoId
                    }
                />
            </ScrollView>


        </View>);
}

export default Explore;
