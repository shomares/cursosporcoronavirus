import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions } from 'react-native';
import { WebView } from 'react-native-webview';
import Constants from 'expo-constants';
import { useTheme} from '@react-navigation/native';


const VideoPlayer = ({route})=> {
    let {videoId, title} = route.params;
    const {colors} = useTheme();
    const mycolor =  colors.iconColor;

    return (
    <View style={{
        flex: 1,
        marginTop : Constants.statusBarHeight
    }}>

        <View style={{
            width : "100%",
            height : 200
        }}>
            <WebView
             javaScriptEnabled = {true}
             domStorageEnabled = {true}
                source = {{
                    uri : `https://www.youtube.com/embed/${videoId}`
                }}
             />

        </View>

        <Text style={{
            fontSize : 20,
            width : Dimensions.get('screen').width - 50 ,
            margin : 9, 
            color : mycolor
        }}
            numberOfLines = {2}
            ellipsizeMode = "tail"
        >
            {title}
        </Text>

        <View style={{
            borderBottomWidth : 1
        }}>

        </View>


    </View>);
}


export default VideoPlayer;