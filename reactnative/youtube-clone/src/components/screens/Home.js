import React from 'react';
import { StyleSheet, Text, View, ScrollView, FlatList, Animated } from 'react-native';
import Header from '../Header';
import Card from '../Card';
import {useSelector} from 'react-redux';

import Constant from 'expo-constants'


export default function HomeScreen() {

  const cardData = useSelector(state => state.cardData);
  const scrollY = new Animated.Value(0);
  const diffClamp = Animated.diffClamp(scrollY, Constant.statusBarHeight, Constant.statusBarHeight+ 45);


  const translateY = diffClamp.interpolate({
    inputRange : [Constant.statusBarHeight, Constant.statusBarHeight + 35],
    outputRange: [Constant.statusBarHeight, Constant.statusBarHeight - 100]
  })



  return (
    <View style= {{
        flex : 1, 
        //marginTop : Constant.statusBarHeight + 45
    }}>
      <Animated.View 
      style= {{
        transform : [
          {
            translateY
          }
        ],
        elevation : 4,
        zIndex: 100
     
      }}>
        <Header />
      </Animated.View>
      <FlatList 
            data = {cardData}
            renderItem = { ({item}) => {
                return <Card
                        videoId = {item.id.videoId}
                        title = {item.snippet.title}
                        channel = {item.snippet.channelTitle}
                />
            }}
            keyExtractor = {
                item => item.id.videoId
            }
            onScroll = { (e) =>{
                scrollY.setValue(e.nativeEvent.contentOffset.y);
              }
            }

            style = {{
              position: "relative"
            }}
        />
     
    </View>
  );
}

