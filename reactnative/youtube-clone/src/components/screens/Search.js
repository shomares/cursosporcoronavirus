import React, {useState} from 'react';
import { StyleSheet, Text, View, ScrollView, TextInput, FlatList} from 'react-native';
import {Ionicons} from '@expo/vector-icons';
import MiniCard from '../MiniCard';
import Youtubeservice from '../../services/youtubeservice'
import {useNavigation, useTheme} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import Constant from 'expo-constants'


const Search  = ()=> {

    const {colors} = useTheme();
    const mycolor =  colors.iconColor;

    const [value, setValue] = useState('');
    const [loading, setLoading] = useState (false);
    const navigation = useNavigation();

    const miniCardData = useSelector ((state) => state.cardData);
    const dispatch = useDispatch();

  

    const fetchData  = async()=> {
        setLoading(true);
        let service = Youtubeservice();
        let data = await service.search(value);
        dispatch({
            type : 'add',
            payload : data.items
        });

        
        setLoading(false);

    };

    return (<View style={{
        flex : 1
       
    }}>
        <View style= {{
            padding : 5,
            flexDirection : "row",
            justifyContent : "space-around",
            elevation : 5,
            backgroundColor :  colors.headerColor,
            marginTop:Constant.statusBarHeight
       
        }}>
            <Ionicons  
                name = "md-arrow-back" 
                size={32}
                onPress = {()=> navigation.goBack()}
                style = {{
                    color : mycolor
                }}
            />
            <TextInput  
                style = {{
                    width : "70%",
                    backgroundColor : "#e6e6e6"
                }}
                value = {value}
                onChangeText = {(value)=> setValue(value) }
            />
            <Ionicons
                name = "md-send"
                size = {32}
                onPress = {()=> fetchData()}
                style = {{
                    color : mycolor
                }}

            />

        </View>

        <FlatList 
            data = {miniCardData}
            renderItem = { ({item}) => {
                return <MiniCard
                        videoId = {item.id.videoId}
                        title = {item.snippet.title}
                        channel = {item.snippet.channelTitle}

                />
            }}
            keyExtractor = {
                item => item.id.videoId
            }
            refreshing = {loading}
            onRefresh = {()=> fetchData()}
        />

    </View>)
};

export default Search;