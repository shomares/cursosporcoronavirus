import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {AntDesign, Ionicons,MaterialIcons} from '@expo/vector-icons';
import {useNavigation, useTheme} from '@react-navigation/native';
import Constant from 'expo-constants'
import {useDispatch} from 'react-redux';
export default function Header() {
    const {colors} = useTheme();
    const mycolor =  colors.iconColor;
    const navigation = useNavigation();
    const dispatch = useDispatch();

  return (
   <View style={{
       height : 45,
       backgroundColor : colors.headerColor,
        flexDirection : "row",
       justifyContent : "space-between",
        marginTop:Constant.statusBarHeight,
        //position:"absolute"
       
   }}>
       <View style={{
           flexDirection : "row",
           margin : 5

       }}>
            <AntDesign 
                    name="youtube"
                    size={28} 
                    style = {{
                        marginLeft : 20
                    }}
                    color="red"
                
             />
            <Text style={{
                fontSize : 22,
                marginLeft : 5,
                fontWeight : "bold",
                color: mycolor
            }}>Youtube </Text>
       </View>
       <View style={{
           flexDirection : "row",
           justifyContent : "space-around",
           width : 150,
           margin : 5

       }}>

        <Ionicons 
            name = "md-videocam"
            size = {32}
            color = {mycolor}
        />
        <Ionicons 
            name = "md-search"
            size = {32}
            color = {mycolor}
            onPress = {()=> navigation.navigate('search')}
        />
        <MaterialIcons 
            name = "account-circle"
            size = {32}
            color = {mycolor}
            onPress = {()=> dispatch({
                type : "changeTheme"
            })}
        />



       </View>
   </View>
  );
}

