import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity } from 'react-native';
import {useNavigation, useTheme} from '@react-navigation/native';
import { color } from 'react-native-reanimated';

const MiniCard = ({videoId, title, channel})=> {
    const navigation = useNavigation();
    const {colors} = useTheme();
    const textColor = colors.iconColor;
    return (
        <TouchableOpacity
        onPress = {()=> navigation.navigate("videoPlayer", {
           videoId,
           title
        })}
       >       
            <View style={
                {
                    flexDirection : "row",
                    margin : 10,
                    marginBottom: 0
                }
            }>
                <Image
                    source = {{uri : `https://i.ytimg.com/vi/${videoId}/hqdefault.jpg`}}
                    style = {{
                        width : "45%",
                        height : 100
                    }}
                />
                <View style= {{
                    paddingLeft : 7
                }}>
                    <Text style= {{
                        fontSize : 17,
                        width : Dimensions.get("screen").width / 2,
                        color: textColor
                        }}
                        ellipsizeMode = "tail"
                        numberOfLines = {3}
                    >
                    {title}
                    </Text>
                    <Text  style= {{
                        fontSize : 12,
                        color: textColor

                    }}>
                    {channel}

                    </Text>
                </View>


            </View>
        </TouchableOpacity>
    )
}

export default MiniCard;