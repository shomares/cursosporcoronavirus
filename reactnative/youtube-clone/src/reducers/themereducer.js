

export const themeReducer = (state = false, action) => {

    switch(action.type)
    {
        case 'changeTheme':
            return  !state
    }

    return state;
}

