import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Home from './src/components/screens/Home';
import Search from './src/components/screens/Search';
import Subscribe from './src/components/screens/Subscribe';
import Explore from './src/components/screens/Explore';
import VideoPlayer from './src/components/screens/VideoPlayer';

import { NavigationContainer, DefaultTheme, DarkTheme, useTheme } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import {MaterialIcons} from '@expo/vector-icons';
import {Provider, useSelector} from 'react-redux';
import {createStore, combineReducers} from 'redux';

import {reducer} from './src/reducers/reducer';
import {themeReducer} from './src/reducers/themereducer';
import Constant from 'expo-constants'


const reducers  = combineReducers({
  cardData : reducer,
  myDarkMode : themeReducer
});


const store = createStore(reducers);
const customDarkTheme = {
  ...DarkTheme,
  colors:{
    ...DarkTheme.colors,
    headerColor : "#404040",
    iconColor : "white",
    tabIcon : "white"
  }
}

const customDefaultColor = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      headerColor : "white",
      iconColor : "black",
      tabIcon : "red"

    }
}


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator ();



const RootHome = ()=> {
  const {colors} = useTheme();
  return (
      <Tab.Navigator
       screenOptions={({ route }) => ({
          tabBarIcon: ({  color  }) => {
            let iconName;

            if (route.name === 'home') {
              iconName = "home"
            } else if (route.name === 'explore') {
              iconName ="explore"
            }else if (route.name === 'subscribe'){
              iconName = "subscriptions";
            }
            

            // You can return any component that you like here!
            return <MaterialIcons name={iconName} size={32} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: colors.tabIcon,
          inactiveTintColor: 'gray',
        }}
      
      >
        <Tab.Screen name="home" component={Home} />
        <Tab.Screen name="explore" component={Explore} />
        <Tab.Screen name="subscribe" component={Subscribe} />
      </Tab.Navigator>
  )

};


const Navigation = () => {
  let currentTheme = useSelector(state=> state.myDarkMode);

  return (
      <NavigationContainer theme= {currentTheme ? customDarkTheme : customDefaultColor}>
      <Stack.Navigator  headerMode="none">
        <Stack.Screen 
            name="rootHome" 
            component={RootHome} 
        />
        <Stack.Screen name="search" component={Search} options= {{
          headerBackTitleVisible : false
        }}/>
        <Stack.Screen name="videoPlayer" component={VideoPlayer}  options= {{
          headerBackTitleVisible : false
        }}/>
        
      </Stack.Navigator>
      </NavigationContainer>
  );
}



export default ()=> {
  return (<Provider store={store}>
      <Navigation/>
  </Provider>)
}

