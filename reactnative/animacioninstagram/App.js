import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, FlatList, Animated, PanResponder, Dimensions, NativeModules, LayoutAnimation } from 'react-native';
import { Card, Button } from 'react-native-paper';
import { AntDesign } from '@expo/vector-icons'
export default function App() {

  const SCREEN_WIDTH = Dimensions.get("screen").width;
  const SWIPE_THRESSHOLD = SCREEN_WIDTH / 2;

  const { UIManager } = NativeModules;
  const [handleable, setHandleable] = useState(0);


  const data = [
    { id: "1", uri: "https://images.unsplash.com/photo-1587670090629-f826a52f808f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjExMDk0fQ&auto=format&fit=crop&w=675&q=80" },
    { id: "2", uri: "https://images.unsplash.com/photo-1491438590914-bc09fcaaf77a?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" },
    { id: "3", uri: "https://images.unsplash.com/photo-1531715047058-33b6c9df7897?ixlib=rb-1.2.1&auto=format&fit=crop&w=1267&q=80" },
    { id: "4", uri: "https://images.unsplash.com/photo-1542596594-649edbc13630?ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80" },
    { id: "5", uri: "https://images.unsplash.com/photo-1587670090629-f826a52f808f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjExMDk0fQ&auto=format&fit=crop&w=675&q=80" },
  ];

  const position = new Animated.ValueXY({
    x: 0,
    y: 0
  });

  UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

  const swiped = (direction) => {
    const x = direction === "rigth" ? SCREEN_WIDTH + 10 : -SCREEN_WIDTH - 10;
    Animated.spring(position, {
      toValue: {
        x,
        y: 0
      }
    }).start(() => {
      position.setValue({ x: 0, y: 0 });
      setHandleable(ant => ant + 1);
      LayoutAnimation.spring();

    });
  }



  const pan = PanResponder.create({
    onStartShouldSetPanResponder: () => true,
    onPanResponderMove: Animated.event([
      null,
      { dx: position.x }
    ]),
    onPanResponderRelease: (e, gesture) => {
      if (gesture.dx > SWIPE_THRESSHOLD) {
        swiped('rigth');
      } else if (gesture.dx < -SWIPE_THRESSHOLD) {
        swiped('left');
      } else {
        Animated.spring(position, {
          toValue: {
            x: 0,
            y: 0
          }
        }).start();
      }

    }
  });

  const rotate = position.x.interpolate({
    inputRange: [-SCREEN_WIDTH, 0, SCREEN_WIDTH],
    outputRange: ["-40deg", "0deg", "40deg"]
  });


  const [like, setLike] = useState(false);
  const [visible, setVisible] = useState(false);
  const [counter, setCouter] = useState(-1);

  const AnimatedIcon = Animated.createAnimatedComponent(AntDesign);

  const currentValue = new Animated.Value(1);
  useEffect(() => {
    if (like) {
      Animated.spring(currentValue, {
        toValue: 2,
        friction: 2
      }).start(() => {
        Animated.spring(currentValue, {
          toValue: 1
        }).start(() => {
          setVisible(false)
        });
      });
    }
  }, [like]);


  const CardView = (item, index) => (
    <Card>
      <Card.Cover source={{ uri: item.uri }} />
      <Card.Actions>
        <AntDesign
          name={like && index === counter ? "heart" : "hearto"}
          size={30}
          color="red"
          onPress={() => {
            if (!like) {
              setVisible(true);
            }
            setLike(!like)
            setCouter(index)
          }}
        />
      </Card.Actions>
    </Card>
  );


  if (handleable < data.length) {
    return (
      <View style={{
        flex: 1
      }}>
        {visible &&
          <AnimatedIcon
            name="heart"
            size={50}
            color="red"
            style={{
              position: "absolute",
              top: 120,
              left: "40%",
              zIndex: 99999,
              transform: [
                { scale: currentValue }
              ]
            }}
          />
        }

        <FlatList
          data={data}
          keyExtractor={item => item.id}
          renderItem={({ item, index }) => {
            if (index < handleable) {
              return null;
            }
            if (index === handleable) {
              return (
                <Animated.View
                  {...pan.panHandlers}
                  style={{
                    transform: [
                      { translateX: position.x },
                      { rotate: rotate }
                    ]
                  }}>
                  {CardView(item, index)}
                </Animated.View>
              );
            } else {
              return (
                <Animated.View style={{
                  transform: [

                  ]
                }}>
                  {CardView(item, index)}
                </Animated.View>

              );
            }

          }

          }


        />
      </View>
    );
  }else{
    return <Button onPress = {()=> setHandleable(0)}>Reset</Button>
  }
}

