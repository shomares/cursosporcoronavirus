import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack';
import ListNotes from './screens/ListNotes';
import ShowNote from './screens/ShowNotes';
import EditNote from './screens/EditNode';

import {NotesProvider} from './context/NotesContext';
import CreateNotes from './screens/CreateNotes';
const Stack = createStackNavigator();




const App = () =>{
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen 
           name="notes" 
           component= {ListNotes} 
           options = {{
             title : "All Notes",
             headerTitleAlign : "center"
           }}
            

        />

      <Stack.Screen 
           name="create" 
           component= {CreateNotes} 
           options = {{
             title : "Create notes",
             headerTitleAlign : "center"
           }}
            

        />

    <Stack.Screen 
           name="show" 
           component= { ShowNote} 
           options = {{
             title : "Note",
             headerTitleAlign : "center"
           }}
            

        />


    <Stack.Screen 
           name="edit" 
           component= { EditNote} 
           options = {{
             title : "Edit Note",
             headerTitleAlign : "center"
           }}
            

        />

        
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default ()=> {
  return (
    <NotesProvider>
      <App/>
    </NotesProvider>
    
  )
}

