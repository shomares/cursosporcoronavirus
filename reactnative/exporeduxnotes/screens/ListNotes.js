import React, { useContext } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity } from 'react-native';
import { NotesContext } from '../context/NotesContext';
import { REMOVE } from '../reducers/NotesReducer';
import { AntDesign } from '@expo/vector-icons';
const ListNodes = ({ navigation }) => {
    const { state, dispatch } = useContext(NotesContext);
    return (
        <View style={
            {
                flex: 1
            }
        }>
            <View style={{
                alignItems: "center"
            }}>
                <TouchableOpacity
                    onPress={() =>
                        navigation.navigate('create')
                    }
                    style={{
                        marginVertical: 10,
                        backgroundColor: "blue",
                        width: 60,
                        height: 60,
                        borderRadius: 30,
                        alignItems: "center",
                        justifyContent: "center"
                    }}
                >
                    <AntDesign size={30} name="plus" color="white" />
                </TouchableOpacity>
            </View>

            <FlatList
                data={state}
                keyExtractor={item => item.title}
                renderItem={({ item }) => {
                    return (
                        <TouchableOpacity
                            onPress = {()=> navigation.navigate("show", {id : item.id})}
                        >
                            <View style={{
                                flexDirection: "row",
                                justifyContent: "space-between",
                                marginHorizontal: 10,
                                padding: 10,
                                elevation: 4,
                                marginBottom: 5,
                                backgroundColor: "white"
                            }}>
                                <Text style={{
                                    fontSize: 20
                                }}>{item.title}</Text>
                                <AntDesign size={24} name="delete"
                                    onPress={() =>
                                        dispatch({
                                            type: REMOVE,
                                            payload: item.id
                                        })

                                    }
                                />
                            </View>
                        </TouchableOpacity>
                    )
                }}


            />
        </View>
    )
};

export default ListNodes;

