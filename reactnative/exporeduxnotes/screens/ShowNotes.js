import React, { useContext } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { NotesContext } from '../context/NotesContext';
import { Feather } from '@expo/vector-icons';

const ShowNote = ({ route, navigation }) => {
    const { id } = route.params;
    const { state } = useContext(NotesContext);

    const note = state.find(record => record.id === id);

    return (
        <View style={{
            flex: 1,
            marginTop: 10
        }}>

            <View style={{
                alignItems: "center"
            }}>
                <TouchableOpacity
                    onPress={() =>
                        navigation.navigate('edit', {id})
                    }
                    style={{
                        marginVertical: 10,
                        backgroundColor: "blue",
                        width: 60,
                        height: 60,
                        borderRadius: 30,
                        alignItems: "center",
                        justifyContent: "center"
                    }}
                >
                    <Feather size={30} name="edit" color="white" />
                </TouchableOpacity>
            </View>

            <Text style={style.myText}>{note.title}</Text>
            <Text style={style.myText}>{note.content}</Text>
        </View>
    )
}

const style = StyleSheet.create({
    myText: {
        fontSize: 22,
        textAlign: "center"
    }
})

export default ShowNote;