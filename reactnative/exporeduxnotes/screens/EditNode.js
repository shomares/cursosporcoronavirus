import React, { useState, useContext } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { NotesContext } from '../context/NotesContext';
import { UPDATE } from '../reducers/NotesReducer';


const EditNotes = ({ navigation, route }) => {

    const { state, dispatch } = useContext(NotesContext);
    const {id}  = route.params;
    const note = state.find(val => val.id === id);


    const [title, setTitle] = useState(note.title);
    const [content, setContent] = useState(note.content);

    
    return (
        <View style={{
            flex: 1,
            margin: 3
        }}>
        
            <Text style={{
                fontSize: 22
            }}>Update Title:</Text>
            <TextInput
                value={title}
                onChangeText={(val) => setTitle(val)}
                style={style.input}
            />

            <Text style={{
                fontSize: 22
            }}>Update Content:</Text>
            <TextInput
                value={content}
                onChangeText={(val) => setContent(val)}
                style={style.input}
                numberOfLines={3}
                multiline={true}

            />

            <TouchableOpacity style={{
                backgroundColor: "blue",
                padding: 12,
                marginHorizontal: 30,
                borderRadius: 10,
                marginTop: 30,
            }}

                onPress={() => {
                    dispatch({
                        type: UPDATE,
                        payload: {
                            title,
                            content,
                            id
                        }
                    });
                    navigation.navigate("notes");

                }

                }>
                <Text style={{
                    fontSize: 22,
                    color: "white",
                    textAlign: "center",

                }}>Update Note</Text>

            </TouchableOpacity>

        </View>
    )
};


const style = StyleSheet.create({
    input: {
        fontSize: 22,
        borderWith: 1,
        borderColor: "black",
        marginVertical: 8
    }
});

export default EditNotes;