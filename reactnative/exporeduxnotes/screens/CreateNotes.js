import React, { useState, useContext } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { NotesContext } from '../context/NotesContext';
import { ADD } from '../reducers/NotesReducer';


const CreateNotes = ({ navigation }) => {
    const [title, setTitle] = useState("");
    const [content, setContent] = useState("");
    const { state, dispatch } = useContext(NotesContext);

    return (
        <View style={{
            flex: 1,
            margin: 3
        }}>
            <Text style={{
                fontSize: 22
            }}>Enter Title:</Text>
            <TextInput
                value={title}
                onChangeText={(val) => setTitle(val)}
                style={style.input}
            />

            <Text style={{
                fontSize: 22
            }}>Enter Content:</Text>
            <TextInput
                value={content}
                onChangeText={(val) => setContent(val)}
                style={style.input}
                numberOfLines={3}
                multiline={true}

            />

            <TouchableOpacity style={{
                backgroundColor: "blue",
                padding: 12,
                marginHorizontal: 30,
                borderRadius: 10,
                marginTop: 30,
            }}

                onPress={() => {
                    dispatch({
                        type: ADD,
                        payload: {
                            title,
                            content
                        }
                    });
                    navigation.goBack();

                }

                }>
                <Text style={{
                    fontSize: 22,
                    color: "white",
                    textAlign: "center",

                }}>Save</Text>

            </TouchableOpacity>

        </View>
    )
};


const style = StyleSheet.create({
    input: {
        fontSize: 22,
        borderWith: 1,
        borderColor: "black",
        marginVertical: 8
    }
});

export default CreateNotes;