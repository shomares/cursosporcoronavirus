export const initialState = [
    {
        title: "Title 1", id: 1 , content : "asasadeqqew"
    },
    {
        title: "Title 2", id: 2, content : "asasadeqqew"
    },
    {
        title: "Title 3", id: 3, content : "asasadeqqew"
    }

]

export const ADD = "ADD";
export const REMOVE = "REMOVE";
export const UPDATE = "UPDATE";



export const reducer = (state = [], {type, payload}) => {

    switch (type) {
        case ADD:
            return [...state, { id: Math.random(), ...payload }];
        case REMOVE:
            return state.filter(note => payload !== note.id);
        case UPDATE:
            return state.map(record => {
                return record.id === payload.id ? {
                    ...payload
                } : 
                record;
            })
            

    }
    return state;
}