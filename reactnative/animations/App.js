import React from 'react';
import { StyleSheet, Text, View, Animated, PanResponder } from 'react-native';

export default function App() {
  const position = new Animated.ValueXY({ x: 0, y: 0 });

  /*
  Animated.timing(position, {
    toValue : {
      x:200,
      y: 500
    },
    duration: 2000
  }).start();
  */




  const pan = PanResponder.create({
    onMoveShouldSetPanResponder: () => true,
    onPanResponderMove: (e, gesture)=> {
        position.setValue({
          x : gesture.dx,
          y : gesture.dy
        })
    }
    
    /*Animated.event([
      null,
      {
        dx : position.x,
        dy : position.y
      }
    ])
    */
    ,
    onPanResponderRelease : ()=> {
     
        Animated.spring(position, {
          toValue : {x:0, y:0}
        }).start();
    }
  });


  const rotate = position.x.interpolate({
    inputRange: [0, 100],
    outputRange: ["0deg", "90deg"]
  });


  return (
    <View style={{
      flex: 1,
      justifyContent : "center",
      alignItems : "center"
    }}>
      <Animated.View
        {...pan.panHandlers}

        style={{
          height: 80,
          width: 80,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "red",
          transform: [
            {
              translateX: position.x,
            },

            {
              translateY: position.y
            },
            {
              rotate
            }
          ]

        }}>
        <Text>Hello</Text>
      </Animated.View>
    </View>
  );
}

