import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import Search from './screens/Search';
import Home from './screens/Home';
import weatherReducer from './reducers/weatherreducer';
import  MaterialCommunityIcons  from 'react-native-vector-icons/MaterialCommunityIcons';

import {createStore} from 'redux';
import {Provider} from 'react-redux';


import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const Tab = createBottomTabNavigator();

const store = createStore(weatherReducer);

const App = () => {
  return (
    <>
    <Provider store = {store}>
      <StatusBar barStyle="dark-content" backgroundColor="#00aaff" />
        <NavigationContainer>
          <Tab.Navigator
            screenOptions={({ route }) => ({
              tabBarIcon: ({ color }) => {
                let iconName;

                if (route.name === "home") {
                  iconName = "home-city"
                } else if (route.name === "search") {
                  iconName = "city"
                }

                return (<MaterialCommunityIcons name = {iconName} size= {25} color={color}/>)
              }
            })
            }
            tabBarOptions={{
              activeTintColor: "white",
              inactiveTintColor: "gray",
              activeBackgroundColor : "#00aaff",
              inactiveBackgroundColor : "#00aaff"

            }}
          >
            <Tab.Screen name="home" component={Home} 
              initialParams = {{city : "london"}}
            />
            <Tab.Screen name="search" component={Search} />
          </Tab.Navigator>
        </NavigationContainer>
      </Provider>
    </>
  );
};



export default App;
