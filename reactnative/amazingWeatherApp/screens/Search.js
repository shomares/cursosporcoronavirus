import React, { useState } from 'react';
import { TextInput, Card } from 'react-native-paper';
import { View, Text, FlatList  } from 'react-native'
import Header from './Header';
import { Button } from 'react-native-paper';
import Service from '../services/WeatherService';
import { useDispatch } from 'react-redux';
import Action from '../actions/weatheraction';



export default Search = ({navigation}) => {
    const [city, setCity] = useState('');
    const [cities, setCities] = useState([]);

    const dispacher = useDispatch();

    
    const fetchCities = async (text)=> {
        setCity(text);
        let service = Service();
        let result = await service.getCities(text);
        setCities(result.RESULTS.slice(0, 9));
    };


    const clickButton = async ()=> {
        let action = Action();
        let result = await action.updateWeather(city);
        dispacher(result);
        navigation.navigate('home');
    }

    const listClick = async (name)=> {
        setCity(name);
        let action = Action();
        let result = await action.updateWeather(name);
        dispacher(result);
        navigation.navigate('home');
    };



    return (
        <View style={{
            flex: 1
        }}>
            <Header name="Search Screen" />
            <TextInput
                label="city name"
                theme={{
                    colors: {
                        primary: "#00aaff"
                    }
                }}
                value={city}
                onChangeText={(text) => fetchCities(text)}

            />


            <Button  
                    icon = "content-save"
                    mode="contained" 
                    theme = {{
                            colors: {
                            primary: "#00aaff"
                         }
                    }}
                    style = {{
                        margin : 20
                    }}
                    onPress={() => clickButton()}
            >
                <Text style={{
                    color : "white"
                }}>
                  Save changes
                </Text>
            </Button>

            <FlatList
                data = {cities}
                renderItem = {({item})=>{
                    return (
                        <Card 
                            onPress = { ()=> listClick(item.name)}
                             style = {{
                                 margin : 2,
                                 padding : 12
                             }}
                        >
                            <Text>{item.name}</Text>
                        </Card>
                    )
                }}
                keyExtractor = {item=>item.name}
            />
       
        </View>
    );
};