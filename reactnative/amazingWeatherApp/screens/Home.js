import React, { useState, useEffect } from 'react';
import { TextInput, Card, Title } from 'react-native-paper';
import { View, Text, FlatList, Image  } from 'react-native'
import Header from './Header';
import Action from '../actions/weatheraction';
import { useSelector, useDispatch } from 'react-redux';


export default  Home = ()=> {
     const info = useSelector(state => state);
     const dispacher = useDispatch();


    const getWeather = async ()=> {
        let action = Action();
        let city = await action.getCity();
        city = city ? city : 'london';
        let result = await action.updateWeather(city);
        dispacher(result);
    };

    useEffect(()=> {
        getWeather();
    }, [])
   

    return (<View style={{
        flex: 1
    }}>
           <Header name="Weather"/> 
           <View
            style = {{
                alignItems : "center"
            }}
           >
               <Title
                style = {{
                    color : '#00aaff',
                    marginTop : 30,
                    fontSize : 30
                }}
               >
                   {info.name}
               </Title>
               <Image
                    style= {{
                        width : 120,
                        height : 120,
                    }}
                    source = {{
                        uri : `https://openweathermap.org/img/w/${info.icon}.png`
                    }}

               />
           </View>
           <Card
            style = {{
                margin : 5,
                padding : 12
            }}
           >
                <Title
                style = {{
                    color : "#00aaff"
                }}
                >
                    Temperature - {info.temp}
                </Title>
            </Card>

            <Card
            style = {{
                margin : 5,
                padding : 12
            }}
           >
                <Title
                style = {{
                    color : "#00aaff"
                }}
                >
                    Humidity - {info.humidity}
                </Title>
            </Card>

            <Card
            style = {{
                margin : 5,
                padding : 12
            }}
           >
                <Title
                style = {{
                    color : "#00aaff"
                }}
                >
                Description - {info.desc}
                </Title>

           </Card>

    </View>)
};
