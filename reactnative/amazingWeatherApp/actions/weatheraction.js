import Service from '../services/WeatherService';
import {UpdateWeather} from '../reducers/weatherreducer';
import AsyncStorage from '@react-native-community/async-storage';

const updateWeather = async (city) => {
    const service =  Service();
    const result = await service.getWeather(city);
    await AsyncStorage.setItem('newCity', city);
   
    return {
        type : UpdateWeather,
        payload :{
            name : result.name,
            temp : result.main.temp,
            humidity: result.main.humidity,
            desc: result.weather[0].description,
            icon : result.weather[0].icon
        }

    }
}


const getCity = async ()=> {
    return await AsyncStorage.getItem('newCity');
}


export default Action = ()=> ({
    updateWeather,
    getCity
});