export const UpdateWeather = "UPDATEWEATHER";
export default weatherReducer = (state = {
    name: "loading",
    temp: "loading",
    humidity: "loading",
    desc: "loading",
    icon: "loading"
}, action) => {
    switch (action.type) {
        case UpdateWeather:
            return { ...action.payload }
    }

    return state;
}