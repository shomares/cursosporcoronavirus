const getCities = async (city)=> {
    let data = await fetch(`https://autocomplete.wunderground.com/aq?query=${city}`);
    return await data.json();
}


const getWeather = async(city) => {
    let data = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=b21ad83f871771bb8fbd2fc4b608363b&units=metric`);
    return await data.json();
}

const module = ()=> ({
    getCities,
    getWeather
});

export default module;