from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from flask_mysqldb import MySQL
import yaml
db = yaml.load(open('db.yaml'))


app = Flask(__name__)
app.config['MYSQL_HOST'] = db['mysql_host']
app.config['MYSQL_USER'] = db['mysql_user']
app.config['MYSQL_PASSWORD'] = db['mysql_password']
app.config['MYSQL_DB'] = db['mysql_database']


mysql = MySQL(app)

Bootstrap(app)

@app.route('/')
def index():
    fruits = ['mango', 'fresa']
    cur = mysql.connection.cursor()
    cur.execute('INSERT INTO Employe VALUES (%s)', ['Test'])
    mysql.connection.commit()
    result = cur.execute('select * from Employe')
    if result > 0:
        users = cur.fetchall()

    return render_template('index.html', fruits = fruits, users = users)




if __name__ == '__main__':
    app.run(debug=True)
