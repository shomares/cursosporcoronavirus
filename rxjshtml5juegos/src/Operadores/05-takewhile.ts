import { fromEvent } from 'rxjs';
import { map, first, takeWhile } from 'rxjs/operators';

interface Position{
    clientX : number;
    clientY : number
}

const sourceFromClick$ = fromEvent<MouseEvent>(document, 'click').pipe(
    map<MouseEvent, Position>(({clientX, clientY}) => ({
        clientX, 
        clientY
    })),
    takeWhile(({clientY}) => clientY >150, true)
);

sourceFromClick$.subscribe({
    next : (value)=> console.log('next', value),
    complete : ()=> console.log('finish')
});


