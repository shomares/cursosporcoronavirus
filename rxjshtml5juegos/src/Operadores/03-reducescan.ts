import {interval} from 'rxjs';
import { take, reduce, tap, scan } from 'rxjs/operators';

const sourceInterval$ = interval(500);

const reducer = (acc: number, current:number)=> acc + current;

const sourceSuma$ = sourceInterval$.pipe(
    take(5),
    tap(console.log),
    reduce(reducer)
);

sourceSuma$.subscribe(s=>console.log('Reduce :', s));

const sourceScan$ = sourceInterval$.pipe(
    take(3),
    scan(reducer)
)

sourceScan$.subscribe(s=> console.log('Scan : ', s));