import { fromEvent } from "rxjs";
import { map, tap, pluck } from "rxjs/operators";

const divs = document.createElement('div');

divs.innerHTML = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae quam in mauris tempus viverra. Ut auctor magna sed quam placerat dictum. Nunc at justo egestas, convallis ante tristique, consequat ante. Nulla nec turpis vitae nulla interdum scelerisque. Aliquam erat volutpat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec ac neque augue. Ut congue iaculis ante, eu maximus sem faucibus nec. Maecenas fermentum pharetra pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sit amet imperdiet erat, sed vehicula nulla. Nunc pulvinar elit ut lectus dapibus, sed tincidunt metus facilisis. Morbi eu suscipit orci, tempor laoreet massa. Fusce quis massa eu velit porttitor gravida. In imperdiet enim eget urna sagittis, id placerat mi lobortis.
<br/>
<br/>
Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur sollicitudin sodales lectus sed commodo. Suspendisse blandit congue dui sit amet lacinia. Pellentesque at efficitur orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam eget ipsum ultrices, viverra tortor sed, fermentum neque. Proin faucibus semper ipsum. Nunc non dapibus purus. Aenean fermentum placerat ligula vel ultrices. Phasellus sed facilisis sapien. Nullam imperdiet tellus maximus diam accumsan tristique. Suspendisse placerat, augue eu ultricies fermentum, dolor nunc luctus nibh, sed porta arcu ante at nibh. In ullamcorper dui velit, id sodales tortor ultrices et.
<br/>
<br/>
Suspendisse eu ante eget velit tempor euismod. Pellentesque viverra dui quis varius ullamcorper. Morbi mattis leo est, id pulvinar nisi porta quis. In vel ullamcorper ante, eu feugiat urna. Nam id vulputate elit, sed finibus dolor. Nullam posuere posuere nulla eu pulvinar. Praesent et molestie nisi. Phasellus viverra nec ligula pharetra sagittis. Donec eget nisi erat. Praesent massa diam, luctus eget purus in, venenatis pharetra mi. Integer fringilla metus posuere quam congue, et consequat ex elementum. Etiam erat ante, dictum in auctor at, vehicula a neque. Maecenas in enim dictum, accumsan quam sit amet, luctus nisl.
<br/>
<br/>
Nam est turpis, feugiat quis velit tincidunt, dignissim posuere risus. Pellentesque vitae vehicula est. Nunc viverra sem id tellus convallis, non fringilla mi iaculis. Duis aliquam ex vitae mi molestie vulputate. Fusce ultricies ante aliquam ante suscipit, id viverra metus varius. Nullam consectetur imperdiet magna a dictum. In aliquet ornare gravida. Nullam feugiat congue leo, sed bibendum quam commodo a. Sed tincidunt quam eget purus posuere commodo.
<br/>
<br/>
Aliquam et libero augue. Morbi pharetra in dui sit amet tempor. Donec in ligula egestas, semper justo id, iaculis dolor. Quisque sit amet ullamcorper orci. Praesent eu ex varius, sollicitudin metus in, placerat odio. Suspendisse eget finibus ipsum. Nam congue ligula eu felis bibendum, malesuada lobortis magna porttitor. Suspendisse faucibus blandit eros. Maecenas lobortis id ante vitae placerat. Pellentesque id eros sit amet mauris dignissim bibendum ac pellentesque dui. Cras placerat sit amet leo eu feugiat. Nulla facilisi. Donec magna ipsum, venenatis sed interdum ac, hendrerit eget purus. Etiam bibendum enim ex, eget posuere arcu tristique at. Morbi massa lorem, molestie vitae porta vitae, pharetra ultrices augue. Nunc quis lobortis sem.
<br/>
<br/>
Nam est dolor, maximus eget lacus nec, malesuada vehicula justo. Sed in blandit diam. Nam vulputate diam ac libero varius aliquet. Fusce nunc erat, egestas vel sollicitudin id, convallis id velit. Nunc in aliquet justo. Donec convallis laoreet arcu ac aliquet. Pellentesque rhoncus in odio non ultricies. Praesent ex nunc, bibendum nec consectetur ac, varius quis massa. Mauris egestas mi eget consectetur vulputate. Suspendisse non nisl non mi luctus venenatis. Phasellus mollis eros non cursus varius. Sed eu facilisis tortor, vel gravida sem. Nam consequat lectus nunc. Cras faucibus, neque ac consequat fringilla, dui tortor dictum velit, non convallis nisi libero eu neque. Curabitur interdum metus vitae placerat ornare. Fusce luctus id massa ut aliquet.
<br/>
<br/>
Fusce at sapien placerat erat faucibus aliquam a sed est. In volutpat nec ligula eu vestibulum. Aenean blandit iaculis lacus, at pulvinar nibh. Duis maximus ultrices lorem, et varius est lobortis quis. Aliquam iaculis odio ac varius dignissim. Quisque et ante ut elit sodales elementum id sed velit. Morbi volutpat mi vitae justo ornare, a lobortis diam molestie. Phasellus volutpat mauris et metus iaculis commodo. Nullam euismod velit a mi eleifend, dignissim consequat urna posuere. In hendrerit arcu tortor, vitae molestie nisi malesuada eget. Maecenas dignissim justo vel semper varius.
<br/>
<br/>
Donec eu ipsum sed ante ultricies placerat. Duis convallis lectus ac mollis eleifend. Maecenas iaculis pellentesque consequat. Donec ultricies, nibh eu viverra eleifend, nisi diam commodo tellus, fermentum congue nulla leo vel nulla. Aenean vitae ipsum sit amet ante pretium consectetur at sed lorem. Quisque venenatis turpis et iaculis tincidunt. Pellentesque nisi ex, luctus id malesuada in, efficitur pretium justo. Donec faucibus pretium lectus, eget fermentum nibh condimentum vel. Suspendisse at facilisis ligula.
<br/>
<br/>
Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi rutrum rutrum metus vel vulputate. Quisque id orci hendrerit purus laoreet semper. In turpis ipsum, interdum vitae velit vitae, mattis fermentum odio. Nulla facilisi. Phasellus at ultricies odio. Morbi venenatis lectus lacus, quis tincidunt ipsum sollicitudin quis. Maecenas scelerisque neque nec tincidunt sodales. Suspendisse fermentum vitae nulla eu semper. Pellentesque sagittis felis at euismod ullamcorper. Nullam pellentesque facilisis venenatis. Sed ante tellus, congue non ornare ut, efficitur ac magna. Integer lorem eros, consequat quis tempor at, sollicitudin quis magna. Aenean aliquam, turpis vel ornare ornare, tortor erat placerat metus, eu varius nisl enim quis enim. Donec elit lorem, rutrum eu sem non, ornare lacinia nisl. Vivamus euismod mollis sem a sodales.
<br/>
<br/>
Ut sit amet semper felis. Suspendisse faucibus odio ut molestie porta. Vestibulum ipsum leo, tincidunt quis erat vel, ornare porttitor metus. Fusce feugiat auctor ornare. Curabitur id mauris sit amet mi sollicitudin maximus. Ut in ante nec nunc rhoncus sodales. In vulputate nisl ac congue ullamcorper. Nunc semper massa non viverra luctus. Vivamus commodo velit eget aliquam tincidunt. Duis ut ex ut massa tincidunt malesuada. In posuere aliquam risus, a suscipit orci varius nec. Ut feugiat ullamcorper consectetur. Aenean erat risus, luctus sit amet sodales in, bibendum vitae est. Praesent ornare porttitor quam eget malesuada. Donec non mollis dolor. Morbi ut erat a erat ornare rhoncus vel id ante.`;

document.body.appendChild(divs);

const progressBar = document.createElement('div');
progressBar.setAttribute('class', 'progress-bar');
document.body.appendChild(progressBar);



interface ScrollData {
    scrollHeight : number;
    clientHeight : number;
    scrollTop: number
}

const calcular =  ({scrollTop, clientHeight, scrollHeight}: ScrollData): number =>{
    return scrollTop / (scrollHeight - clientHeight);
}

const scrollEvent$ = fromEvent(document, 'scroll')
.pipe(
    pluck('target', 'documentElement'),
    map<any, ScrollData>(s=>{
        return {
            clientHeight : s.clientHeight,
            scrollHeight : s.scrollHeight,
            scrollTop : s.scrollTop
        }
    }),
    map(calcular),
    tap(console.log),
    map(s=> s * 100),
    tap(console.log),
    map(s=> `${s}%`)
    
);

scrollEvent$.subscribe(s=> {
    progressBar.style.width = s;
});