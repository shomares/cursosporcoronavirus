import { fromEvent } from 'rxjs';
import { map, first } from 'rxjs/operators';

interface Position{
    clientX : number;
    clientY : number
}

const sourceFromClick$ = fromEvent<MouseEvent>(document, 'click').pipe(
    map<MouseEvent, Position>(({clientX, clientY}) => ({
        clientX, 
        clientY
    })),
    first<Position>(({clientY})=> clientY > 150 )
);

sourceFromClick$.subscribe(console.log);


