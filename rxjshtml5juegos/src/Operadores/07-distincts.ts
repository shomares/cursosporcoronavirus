import {of, from} from 'rxjs';
import { distinct, distinctUntilChanged, distinctUntilKeyChanged } from 'rxjs/operators';

const source$ = of(1,1,2,3,4,5,5,1,3);

source$.pipe(
    distinct()
).subscribe(s=> console.log('distinct' , s));

source$.pipe(
    distinctUntilChanged()
).subscribe(s=> console.log('distinctUntilChanged', s))

///------------------------------------
interface Album{
    nombre : string
}

const albumes : Album[] = [
    {nombre : "Public Enemy"},
    {nombre : "Otro"},
    {nombre : "Public Enemy"},
    {nombre : "Public Enemy"},
    {nombre : "Metallica"},
    {nombre : "Korn"},
    {nombre : "Korn"},
    {nombre : "Otro"},
];

const sourceFromAlbumes$ = from(albumes);

sourceFromAlbumes$.pipe(distinct(s=>s.nombre))
                  .subscribe(s=> console.log('distinct ', s));

sourceFromAlbumes$.pipe(
    distinctUntilChanged((ant, current)=> ant.nombre === current.nombre)
).subscribe(s=> console.log('distinctUntilChanged', s));


sourceFromAlbumes$.pipe(
    distinctUntilKeyChanged('nombre')
).subscribe(s=> console.log('distinctUntilKeyChanged ', s));