import {fromEvent} from 'rxjs';
import {map, pluck, mapTo} from 'rxjs/operators';

const source$ = fromEvent<KeyboardEvent>(document, 'keydown');

const mapSource$ = source$.pipe(
    map<KeyboardEvent, string>(s=> s.code)
);

const pluckSource$ = source$.pipe(
    pluck<KeyboardEvent, string>('key')
);

const pluckSourceComplex$ = source$.pipe(
    pluck<KeyboardEvent, string>('target', 'innerText')
)

const mapToSource$ = source$.pipe(
    mapTo<KeyboardEvent, String>('CLick')
)


source$.subscribe(console.log);
mapSource$.subscribe(s=> console.log('map ', s));
pluckSource$.subscribe(s=> console.log('plucksimple', s))
pluckSourceComplex$.subscribe(s=> console.log('pluckSourceComplex', s))
mapToSource$.subscribe(s=> console.log('mapto ', s));