import { interval, fromEvent } from 'rxjs';
import { takeUntil, skip, tap } from 'rxjs/operators';

const button = document.createElement('button');
button.innerHTML = 'Stop it!'
document.querySelector('body').append(button);

const interval$ = interval(1000);

const clickButton$ = fromEvent(button, 'click');

interval$.pipe(
    takeUntil(clickButton$.pipe(
        tap(()=> console.log('antes de skip')),
        skip(1),
        tap(()=> console.log('despues de skip'))
    ))
)

.subscribe({
    next: (value) => console.log('next: ', value),
    complete : ()=> console.log('finish')
});

