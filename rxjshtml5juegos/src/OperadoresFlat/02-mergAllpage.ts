import { fromEvent, Observable, of } from "rxjs";
import { debounceTime, pluck, map, mergeAll, scan, tap, filter } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { GitUsersResp } from "../Interfaces/GitUsersResp";
import { GitUser } from "../Interfaces/GitUser";
const body = document.querySelector('body');

const input = document.createElement('input');
const listUl = document.createElement('ol');

const buttonPaginaAnterior = document.createElement('button');
const buttonPaginaSiguiente  = document.createElement('button');

buttonPaginaAnterior.innerHTML = 'Anterior';
buttonPaginaSiguiente.innerHTML = 'Despues';

body.append(input);
body.append(buttonPaginaAnterior);
body.append(buttonPaginaSiguiente);
body.append(listUl);



const mostrarResultados = (resultados : GitUser[])=>{
    listUl.innerHTML = "";
    for(let user of resultados)
    {
        let {avatar_url, html_url}  = user;
        let element = document.createElement('li');
        let hypervinculo = document.createElement('a');
        let img = document.createElement('img');
        hypervinculo.href = html_url;
        hypervinculo.target = "_blank";
        hypervinculo.innerHTML = "Ver perfil";
        img.src = avatar_url;
        element.append(hypervinculo);
        element.append(img);
        listUl.append(element);
    }

}

interface Busqueda {
    page : number,
    abuscar : string
}


const makeRequest = (value : Observable<Busqueda>)=>{
    return value.pipe(
        map<Busqueda, Observable<GitUsersResp>>( s=> ajax.getJSON(`https://api.github.com/search/users?q=${s.abuscar}&page=${s.page}`)),
        mergeAll(),
        pluck<GitUsersResp, GitUser[]>('items')
    );
};


const eventKeyDown$ = fromEvent<KeyboardEvent>(input, 'keyup');
const eventClickAnt$ = fromEvent<MouseEvent>(buttonPaginaAnterior, 'click');
const eventClickDesp$ = fromEvent<MouseEvent>(buttonPaginaSiguiente, 'click');

const state$ = new Observable<number>(subscriber =>{
    eventClickAnt$.subscribe(() => {
        subscriber.next(-1);
    });

    eventClickDesp$.subscribe(() => {
        subscriber.next(1);
    });
});



state$.pipe(
    scan((acc, val) => acc + val< 1 ? 1 : acc + val, 1),
    tap(console.log),
    map<number, Busqueda>( (page) => ({
        abuscar : input.value,
        page
    })),
    map<Busqueda, Observable<GitUser[]>>(s=> makeRequest(of<Busqueda>(s))),
    mergeAll()
).subscribe(mostrarResultados);


eventKeyDown$.pipe(
    debounceTime(500),
    pluck<KeyboardEvent, string>('target', 'value'),
    map<string, Busqueda>(abuscar => ({
        abuscar,
        page : 1
    })),
    map<Busqueda, Observable<GitUser[]>>(s=> makeRequest(of<Busqueda>(s))),
    mergeAll()
).subscribe(mostrarResultados);



