import { fromEvent, interval } from "rxjs";
import { mergeMap, takeUntil } from "rxjs/operators";

const clickdown$ = fromEvent<MouseEvent>(document, 'mousedown');
const clickup$ = fromEvent<MouseEvent>(document, 'mouseup');
const interval$ = interval();

clickdown$.pipe(
    mergeMap(()=> interval$.pipe(
        takeUntil(clickup$)
    )),
)

.subscribe({
    next : (val)=>console.log('Next :', val)
});