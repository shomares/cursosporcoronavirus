import { fromEvent, interval } from "rxjs";
import {  take, exhaustMap } from "rxjs/operators";

const mouseclick$ = fromEvent(document, 'click');
const interval$ = interval(1000).pipe(take(3));

mouseclick$.pipe(
    exhaustMap(()=> interval$)
).subscribe(console.log)