import { fromEvent } from "rxjs";
import { pluck, switchMap } from "rxjs/operators";
import { ajax } from "rxjs/ajax";

const body = document.querySelector('body');
const input = document.createElement('input');

body.append(input);

const keyupevent$ = fromEvent<KeyboardEvent>(input, 'keyup');
const api = 'https://httpbin.org/delay/1?args';

keyupevent$.pipe(
    pluck('target', 'value'),
    switchMap(s => ajax.getJSON(api + s ))
    
)
.subscribe(console.log);

