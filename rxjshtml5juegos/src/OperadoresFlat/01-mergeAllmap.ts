import { fromEvent, Observable } from "rxjs";
import { debounceTime, pluck, map, mergeAll } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { GitUsersResp } from "../Interfaces/GitUsersResp";
import { GitUser } from "../Interfaces/GitUser";
const body = document.querySelector('body');

const input = document.createElement('input');
const listUl = document.createElement('ol');

body.append(input);
body.append(listUl);

const mostrarResultados = (resultados : GitUser[])=>{
    listUl.innerHTML = "";
    for(let user of resultados)
    {
        let {avatar_url, html_url}  = user;
        let element = document.createElement('li');
        let hypervinculo = document.createElement('a');
        let img = document.createElement('img');
        hypervinculo.href = html_url;
        hypervinculo.target = "_blank";
        hypervinculo.innerHTML = "Ver perfil";
        img.src = avatar_url;
        element.append(hypervinculo);
        element.append(img);
        listUl.append(element);
    }

}


const eventClick$ = fromEvent<KeyboardEvent>(input, 'keyup');

eventClick$.pipe(
    debounceTime(500),
    pluck<KeyboardEvent, string>('target', 'value'),
    map<string, Observable<GitUsersResp>>( s=> ajax.getJSON(`https://api.github.com/search/users?q=${s}`)),
    mergeAll(),
    pluck<GitUsersResp, GitUser[]>('items')
).subscribe(mostrarResultados);



