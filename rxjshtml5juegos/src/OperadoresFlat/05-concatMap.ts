import { fromEvent, interval } from "rxjs";
import { concatMap, take } from "rxjs/operators";

const mouseclick$ = fromEvent(document, 'click');
const interval$ = interval(1000).pipe(take(3));

mouseclick$.pipe(
    concatMap(()=> interval$)
).subscribe(console.log)