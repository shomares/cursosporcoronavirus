import { fromEvent, of } from "rxjs";
import { tap, pluck, map, mergeMap, catchError, switchMap, exhaustMap } from "rxjs/operators";
import { ajax } from "rxjs/ajax";

const body = document.querySelector('body');

const submitButton = document.createElement('button');
const userInput = document.createElement('input');
const passwordInput = document.createElement('input');
const form = document.createElement('form');

userInput.placeholder = "user";
userInput.value = "eve.holt@reqres.in";

passwordInput.placeholder = "contraseña";
passwordInput.type = "password";
passwordInput.value = "cityslicka";

submitButton.innerHTML = "Ingresar!"
submitButton.type = "submit";

form.append(userInput);
form.append(passwordInput);
form.append(submitButton);

body.append(form);

const submitEvent$ = fromEvent(form, 'submit')
                        .pipe(tap(event => event.preventDefault()));


interface UserForm {
    email : string;
    password : string;
}

const makeRequest = (request : UserForm)=>{
    return ajax.post('https://reqres.in/api/login?delay=1', request)
                .pipe(
                    pluck('response', 'token'),
                    catchError(()=> of('xxx-xx'))
                );
};


const observable$ = submitEvent$.pipe(
    pluck('target'),
    map<any, UserForm>(s=> ({
        email : s[0].value,
        password : s[1].value
    }))
);

observable$.pipe(
    exhaustMap(makeRequest)
).subscribe(console.log);

/*
observable$.pipe(
    switchMap(makeRequest)
).subscribe(console.log);


observable$.pipe(
    mergeMap(makeRequest)
).subscribe(console.log);

*/


