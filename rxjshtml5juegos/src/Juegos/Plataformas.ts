import { interval, fromEvent, Observable, zip, of, Subject, merge } from "rxjs";
import { pluck, filter, tap, switchMap, map, take } from "rxjs/operators";
import TableroPlataforma from "./data/TableroPlataforma";
import { Jugador } from "./data/Jugador";
import IMovimientoPlataforma from "./interfaces/IMovimientoPlataforma";

const DibujarCanvas = () => {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext("2d");
    canvas.width = 750;
    canvas.height = 500;


    canvas.classList.add('canvasClasico');
    document.querySelector('body').append(canvas);

    return {
        getContext: () => ctx,
        clearCanvas: () => {
            canvas.width = 750;
            canvas.height = 500;
        },
        getCanvas(){
            return canvas;
        }
    }
};

const GetEventTeclado = (): Observable<string> => {
    return fromEvent<KeyboardEvent>(document, 'keydown')
        .pipe(
            pluck<KeyboardEvent, string>('key'),
            filter(s => s === 'ArrowUp' || s === 'ArrowDown' || s === 'ArrowRight' || s === 'ArrowLeft')
        );
};

const GetEventDejarTeclado = (): Observable<string> => {
    return fromEvent<KeyboardEvent>(document, 'keyup')
        .pipe(
            pluck<KeyboardEvent, string>('key'),
            filter(s => s === 'ArrowUp' || s === 'ArrowDown' || s === 'ArrowRight' || s === 'ArrowLeft')
        );
}



const Play = (FPS: number) => {
    const svc = DibujarCanvas();
    const ctx = svc.getContext();
    const cicloPrincipal$ = interval(1000 / FPS);
    const tecladoEvent$ = GetEventTeclado();
    const tecladoEventDown$ = GetEventDejarTeclado();
    const tablero = new TableroPlataforma(50, 50 , 15 , 10);
    const protagonista = new Jugador(tablero);

    let mousex = 0 , mousey = 0;

    //tablero.draw(ctx);

    const rect = svc.getCanvas().getBoundingClientRect();
    merge(
            fromEvent<MouseEvent>(svc.getCanvas(), 'mousedown').pipe(
                
                map(val => ({val , type : 'mousedown'})),
            ),
            fromEvent<MouseEvent>(svc.getCanvas(), 'mousemove').pipe(
                tap(console.log),
                map(val => ({val , type : 'mousemove'})),
            ),
        
        )
    .pipe(
        map( (val) => ({
            x : val.val.pageX - rect.left ,
            y : val.val.pageY - rect.top,
            type : val.type
            
        }))
    
    ).subscribe(evt =>{
        switch(evt.type)
        {
            case 'mousedown':
                tablero.dibujaBloque(evt.x, evt.y);
                break;
           case 'mousemove':
                 mousex = evt.x;
                 mousey = evt.y;
                
        }
    });

   
    tecladoEvent$.pipe(
        tap( val => {
            switch(val)
            {
                case 'ArrowUp':
                    protagonista.up();
                    break;
                break;
                case 'ArrowLeft':
                    protagonista.izquierda();
                    break;
                case 'ArrowRight':
                    protagonista.derecha();
            }
        })

    ).subscribe(console.log);

    tecladoEventDown$.pipe(
        tap( val=> {
            switch(val)
            {
                case 'ArrowLeft':
                    protagonista.sueltaIzquierda();
                    break;
                case 'ArrowRight':
                    protagonista.sueltaDerecha()
            }
        })

    ).subscribe(console.log)

    cicloPrincipal$.pipe(
        //take(2),
        tap(() => {
            svc.clearCanvas();
        }),
        tap(()=>{
            tablero.draw(ctx);
            protagonista.draw(ctx);
            tablero.muestra(mousex, mousey , ctx);
        })
    )
        .subscribe();

};



export default Play;