const colores = [
    "#FF0000",
    "#800080",
    "#FF8C00",
    "#FFD700",
    "#008000",
    "#00CED1",
    "#0000CD"
];

export default colores;

export const colorPlataforma = [
    '#044f14',
    '#c6892f'
]
