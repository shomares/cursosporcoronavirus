import IDrawableDimensional from "../interfaces/IDrawableDimensional";
import TableroPlataforma from "./TableroPlataforma";

export class Jugador implements IDrawableDimensional {
    x: number;
    y: number;
    heigth: number;
    width: number;

    vy: number;
    vx: number;

    gravedad: number;
    friccion: number;

    salto: number;
    velocidad: number;

    suelo: boolean;

    pulsaDerecha: boolean;
    pulsaIzquierda: boolean;

    velocidadMax: number = 5;

    tablero : TableroPlataforma;

    constructor(tablero : TableroPlataforma) {
        this.x = 100;
        this.y = 100;
        this.width = 50;
        this.heigth = 50;

        this.gravedad = 0.5;
        this.friccion = 0.1;
        this.vy = 0;
        this.vx = 0;
        this.salto = 10;
        this.velocidad = 4;

        this.suelo = false;
        this.pulsaDerecha = false;
        this.pulsaIzquierda = false;
        this.tablero = tablero;


    }

    up() {
        if (this.suelo) {
            this.vy -= this.salto;
            this.suelo = false;
        }
    }

    derecha() {
        this.pulsaDerecha = true;
    }

    izquierda() {
        this.pulsaIzquierda = true;


    }

    sueltaDerecha() {
        this.pulsaDerecha = false;
    }

    sueltaIzquierda() {
        this.pulsaIzquierda = false;

    }


    fisica() {

        //CAÍDA
        if (this.suelo == false) {
            this.vy += this.gravedad;
        }
        else {
            this.vy = 0;
        }


        //VELOCIDAD HORIZONTAL
        //Siempre la refrescamos, para que pueda haber inercia y deslice
        if (this.pulsaIzquierda == true) {
            this.vx = -this.velocidad;
        }

        if (this.pulsaDerecha == true) {
            this.vx = this.velocidad;
        }



        //FRICCÓN

        //Izquierda
        if (this.vx < 0) {
            this.vx += this.friccion;

            //si nos pasamos, paramos
            if (this.vx > 0) {
                this.vx = 0;
            }
        }

        //Derecha
        if (this.vx > 0) {
            this.vx -= this.friccion;

            if (this.vx < 0) {
                this.vx = 0;
            }
        }


        //VEMOS SI HAY COLISIÓN POR LOS LADOS
        //derecha
        if (this.vx > 0 && this.tablero.collision(this.x + this.width + this.vx, (this.y + parseInt( (this.heigth / 2).toString() ) )) ) {
            this.vx = 0;
        }

        //Izquierda
        if (this.vx < 0 &&  this.tablero.collision(this.x, (this.y + parseInt( (this.heigth / 2).toString()  )))) {
            this.vx = 0;
        }


        //ACTUALIZAMOS POSICIÓN
        this.y += this.vy;
        this.x += this.vx;


        //para ver si hay colisión por abajo le sumamos 1 casilla a "y"
        if (this.tablero.collision((this.x + (parseInt( (this.heigth / 2).toString()   ))), (this.y + this.heigth))) {
            this.suelo = true;
        }
        else {
            this.suelo = false;
        }


        //COMPROBAMOS COLISIÓN CON EL TECHO (frena el ascenso en seco)
        if (this.tablero.collision((this.x + (parseInt( (this.width / 2).toString()) )), this.y)) {
            this.vy = 0;
        }



    }






    draw(ctx: CanvasRenderingContext2D): void {
        this.fisica();
        ctx.fillStyle = '#820c01';
        ctx.fillRect(this.x, this.y, this.width, this.heigth);
    }


}