import IDrawableSimple from "../interfaces/IDrawableSimple";
import TableroDefault from './DataTablero';
import Colores from './Colores';
import { from, range, Observable, Subject } from "rxjs";
import { switchMap, map, take, filter, reduce } from "rxjs/operators";
import IMovimiento from "../interfaces/IMovimiento";

export default class Tablero implements IDrawableSimple {


    dataTablero: number[][];

    altoTablero: number;
    anchoTablero: number;
    width: number;
    heigth: number;

    x: number;
    y: number;

    margenSuperior: number;
    

    onUpdate$ : Subject<void>;
    onFinish$ : Subject<void>;

    constructor(width: number, heigth: number) {
        this.refresh();
        this.altoTablero = 20;
        this.anchoTablero = 10;
        this.margenSuperior = 4;

        this.width = width;
        this.heigth = heigth;

        this.x = 0;
        this.y = 0;
        this.onUpdate$ = new Subject();
        this.onFinish$ = new Subject();


    }

    
    getBucle() {
        return range(0, 4).pipe(
            switchMap(indexy => range(0, 4).pipe(
                map(indexx => ({
                    xa: indexx,
                    ya: indexy
                }))
            ))
        );
    }

    isLost (){
        if (this.dataTablero[4].filter((x, index)=>index> 0 && x>0 && index<this.anchoTablero).length > 0)
        {
            this.refresh();
            this.onFinish$.next();

        }
    }

    borrarLinea(){
        let resultado = true;
        for (let ya = this.margenSuperior ; ya< this.altoTablero ; ya++){
            resultado = true;
            for (let xa = 1; xa <= this.anchoTablero  ; xa++){

                if (this.dataTablero[ya][xa] === 0)
                {
                    resultado = false;
                    break;
                }
            }

            if (resultado){
                this.dataTablero[ya] =  [1,0,0,0,0,0,0,0,0,0,0,1]
            }
        }
    }

    agregarElemento(movimiento : IMovimiento){
        let {angulo, ficha, x, y} = movimiento;
        
        let shape = [...ficha[angulo]];
        for(let ya = 0; ya < 4 ; ya++){
            for(let xa = 0; xa < 4 ; xa++){
                    let figura = shape[ya][xa];
                    if (figura !== 0)
                    {
                        this.dataTablero[y + ya - 1 ][x + xa] = figura; 
                    }
            }
        }

        this.borrarLinea();
        this.isLost();
        this.onUpdate$.next();

    }


    getBucleTablero() {

        return range(this.margenSuperior, this.altoTablero - this.margenSuperior)
            .pipe(
                switchMap(indexy =>
                    range(1, this.anchoTablero + 1).pipe(
                        map(indexx => ({
                            x: indexx,
                            y: indexy
                        }))
                    )
                ),
                map(({ x, y }) => ({
                    x,
                    y,
                    shape: this.dataTablero[y][x]
                })),
            );
    }



    isCollision(movimiento: IMovimiento): Observable<boolean> {
        let { ficha, angulo, x, y } = movimiento;
        return this.getBucle().pipe(
            map(({ xa, ya }) => {
                if (ficha[angulo][ya][xa] > 0) {
                    if (this.dataTablero[y + ya][x + xa] > 0) {
                        return true;
                    }
                }

                return false;
            }),
            reduce<boolean, boolean[]>((acc, current) => [...acc, current], []),
            map(values => values.filter(x => x).length > 0)
        )
    }

    cleanLine() {
        this.dataTablero.forEach((y, index) => {
            let result = true;

            if (index > 1 && index < this.altoTablero) {
                result = y.filter( (val, index) => val === 0 && index !== 0 && index< this.anchoTablero).length > 0;

                if (!result) {
                   this.dataTablero[index] =  [1,0,0,0,0,0,0,0,0,0,0,1];
                }
            }

        })

    }

    refresh() {
        this.dataTablero = [];
        from(TableroDefault).subscribe({
            next: (data) => this.dataTablero.push([...data]),
            complete: () => console.log('Tablero: ', this.dataTablero)
        }
        );
    }

    draw(ctx: CanvasRenderingContext2D): void {

        this.getBucleTablero().pipe(
            filter(({ shape }) => shape !== 0)
        ).subscribe(({ x, y, shape }) => {
            ctx.fillStyle = Colores[shape];
            ctx.fillRect((x - 1) * this.width, (y - this.margenSuperior) * this.heigth, this.width, this.heigth);
        });
    }

}