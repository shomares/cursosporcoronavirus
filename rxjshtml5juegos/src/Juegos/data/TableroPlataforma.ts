import IDrawableSimple from "../interfaces/IDrawableSimple";
import data from "./DataTableroPlataformas";
import { from, range } from "rxjs";
import { mergeMap, map } from "rxjs/operators";
import { colorPlataforma } from "./Colores";

export default class TableroPlataforma implements IDrawableSimple{


    dataTablero : Number[][];

    width : number;
    heigth : number;

    anchoTablero : number;
    altoTablero : number;

    constructor(width : number, heigth : number, anchoTablero : number, altoTablero : number){
        this.dataTablero = data;
        this.width = width;
        this.heigth = heigth;
        this.anchoTablero = anchoTablero;
        this.altoTablero = altoTablero;

    }

    getBucle(){
        return range(this.altoTablero).pipe(
            mergeMap( y =>  range(this.anchoTablero).pipe(
                map((x) => ({
                    x,
                    y
                }) )
            ))
        )
    }

    refresh(){
        from(this.dataTablero)
        .subscribe( ds => {
            this.dataTablero.push([...ds]);
        })
            
    }

    collision (x : number, y: number)
    {
        return this.dataTablero[parseInt( (y/this.heigth).toString()  )][ parseInt(  (x/this.width).toString()  )] === 0;
    }

    muestra(x : number, y:number , ctx: CanvasRenderingContext2D)
    {
        let bloqueY = parseInt( (y / this.heigth).toString());
        let bloqueX = parseInt( (x / this.width).toString());
        ctx.fillStyle = '#333333';
        ctx.fillRect( bloqueX * this.width, bloqueY * this.heigth , this.width , this.heigth);
       
    }



    dibujaBloque(x : number, y:number)
    {
        let bloqueY = parseInt( (y / this.heigth).toString());
        let bloqueX = parseInt( (x / this.width).toString());

        this.dataTablero[bloqueY][bloqueX] = this.dataTablero[bloqueY][bloqueX] === 0 ? 2 : 0;
    }

    draw(ctx: CanvasRenderingContext2D): void {


          this.getBucle()
                .pipe(
                    map(({x,y}) => ({
                        x,
                        y,
                        color : this.dataTablero[y][x] === 2 ? colorPlataforma[0] : colorPlataforma[1]
                    }))
                )
               .subscribe(({x,y, color})=>{
                    ctx.fillStyle = color;
                    ctx.fillRect( x * this.width, y * this.heigth , this.width , this.heigth);
               });
    }

}