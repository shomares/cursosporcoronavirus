import TiposPieza from './TiposDePieza';
import Colores from './Colores';
import IDrawableDimensional from '../interfaces/IDrawableDimensional';
import { range, Subject } from 'rxjs';
import { switchMap, map, filter } from 'rxjs/operators';
import IMovimiento from '../interfaces/IMovimiento';

export default class Pieza implements IDrawableDimensional {
    heigth: number;
    width: number;
    x: number;
    y: number;

    tipo: number;
    angulo: number;
    margenSuperior: number;

    fotograma : number;
    retraso : number;

    onMovimiento$ : Subject<IMovimiento>;
    onMover$ : Subject<IMovimiento>;



    constructor( heigth: number, width: number, margenSuperior : number) {
        this.x = 5;
        this.y = 8000;
        this.heigth = heigth;
        this.width = width;

        this.angulo = 1;
        this.margenSuperior = margenSuperior;
        this.nueva();

        this.fotograma = 0;
        this.retraso = 50;

        this.onMovimiento$ = new Subject<IMovimiento>();

        this.onMover$ = new Subject<IMovimiento>();

        this.onMover$.subscribe( ({ angulo , x, y })=> {
            this.angulo = angulo;
            this.x = x;
            this.y = y;
        })
    }

    nueva (){
        this.tipo = Math.floor(Math.random() * 7);
        this.y = 2;
        this.x = 4;
    }

    caer(){
        if (this.fotograma < this.retraso)
        {
            this.fotograma ++;
        }
        else{
            let ficha = TiposPieza[this.tipo];
            this.onMovimiento$.next({
                angulo : this.angulo,
                x : this.x,
                y : this.y + 1,
                ficha,
                tipo : 'caer'
            })

            this.fotograma = 0;
        }
    }

    derecha(){
        let ficha = TiposPieza[this.tipo];
        this.onMovimiento$.next({
            angulo : this.angulo,
            x : this.x + 1,
            y : this.y,
            ficha,
            tipo : 'derecha'
        })
    }

    izquierda(){
        let ficha = TiposPieza[this.tipo];
        this.onMovimiento$.next({
            angulo : this.angulo,
            x : this.x - 1,
            y : this.y,
            ficha,
            tipo : 'izquierda'
        })
    }

    abajo (){
        let ficha = TiposPieza[this.tipo];
        this.onMovimiento$.next({
            angulo : this.angulo,
            x : this.x,
            y : this.y + 1,
            ficha,
            tipo : 'abajo'
        })

    }

    rotar(){
        let angulo = this.angulo;
        let ficha = TiposPieza[this.tipo];

        if (this.angulo < 3)
        {
            angulo++;
        }else
        {
            angulo = 0;
        }

        this.onMovimiento$.next({
            angulo,
            x : this.x,
            y : this.y,
            ficha,
            tipo : 'rotal'
        })

    }

    draw(ctx: CanvasRenderingContext2D): void {
       

        const shape = TiposPieza[this.tipo][this.angulo];

        range(0, 4).pipe(
            switchMap((indexX) => range(0, 4).pipe(
                map(indexY => ({
                    x: indexX,
                    y: indexY
                }))
            )),
            map( ({x,y}) => ({
                x,
                y,
                shape : shape[y][x]
            })),
            filter(({shape}) => shape > 0)
        ).subscribe(({ x, y , shape }) => {
                    ctx.fillStyle =  Colores[shape];
                    ctx.fillRect( (this.x  + x  - 1) * this.width, ( this.y  + y   - this.margenSuperior  ) * this.heigth  , this.width , this.heigth  );
            }
        );

    }



}