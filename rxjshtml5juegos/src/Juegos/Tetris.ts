import { interval, fromEvent, Observable, zip, of, Subject } from "rxjs";
import { pluck, filter, tap, switchMap, map, take } from "rxjs/operators";
import Pieza from "./data/Pieza";
import Tablero from "./data/Tablero";
import IMovimiento from "./interfaces/IMovimiento";

const DibujarCanvas = () => {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext("2d");
    canvas.width = 400;
    canvas.height = 640;


    canvas.classList.add('canvasClasico');
    document.querySelector('body').append(canvas);

    return {
        getContext: () => ctx,
        clearCanvas: () => {
            canvas.width = 400;
            canvas.height = 640;
        }
    }
};

const GetEventTeclado = (): Observable<string> => {
    return fromEvent<KeyboardEvent>(document, 'keydown')
        .pipe(
            pluck<KeyboardEvent, string>('key'),
            filter(s => s === 'ArrowUp' || s === 'ArrowDown' || s === 'ArrowRight' || s === 'ArrowLeft')
        );
};



const Play = (FPS: number) => {
    const svc = DibujarCanvas();
    const ctx = svc.getContext();
    const cicloPrincipal$ = interval(1000 / FPS);
    const tecladoEvent$ = GetEventTeclado();

    const pieza = new Pieza( 40, 40, 4);
    const tablero = new Tablero(40, 40);

    tecladoEvent$.pipe(
        tap(event => {
            switch (event) {
                case 'ArrowUp':
                    pieza.rotar();
                    break;
                case 'ArrowDown':
                    pieza.abajo();
                    break;
                case 'ArrowRight':
                    pieza.derecha();
                    break;
                case 'ArrowLeft':
                    pieza.izquierda();
            }
        })
    ).subscribe(console.log);

    const subjectMov$ = new Subject<[boolean ,IMovimiento]> ();

    tablero.onUpdate$.subscribe(()=>{
        tablero.draw(ctx);
    });

    pieza.onMovimiento$.pipe(
        switchMap( s=> zip(tablero.isCollision(s), of(s)))
    ).subscribe(subjectMov$);
    
    subjectMov$.pipe(
        filter((value) => !value[0]),
        map( (value) => value[1]),
    )
    .subscribe( val => pieza.onMover$.next(val));

    subjectMov$.pipe(
        filter( ( val => val[1].tipo === 'caer'  && val[0])),
    ).subscribe( val => {
        pieza.nueva();
        tablero.agregarElemento(val[1]);
    });

    tablero.onFinish$.subscribe(()=> pieza.nueva())


    //tablero.draw(ctx);


    cicloPrincipal$.pipe(
        tap(() => {
            svc.clearCanvas();
        }),
       tap( ()=> tablero.draw(ctx)),
        tap(() => {
            pieza.caer();
            pieza.draw(ctx);
        })
    )
        .subscribe();

};



export default Play;