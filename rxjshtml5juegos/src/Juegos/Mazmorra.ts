import { from, fromEvent, interval, Subject, timer, merge } from "rxjs";
import { switchMap, map, pluck, filter, tap, delay, debounceTime, delayWhen, mergeMap } from "rxjs/operators";

(() => {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext("2d");
    const FPS = 50;

    canvas.width = 750;
    canvas.height = 500;
    canvas.classList.add('canvasClasico');

    document.querySelector('body').append(canvas);

    interface DrawableSimple {
        draw(ctx: CanvasRenderingContext2D): void;
    }

    interface Position {
        x: number,
        y: number
    }

    interface Drawable extends DrawableSimple {
        x: number;
        y: number;
    }

    class Antorcha implements Drawable {
        x: number;
        y: number;

        retraso : number = 10;
        contador : number = 0;
        fotograma : number = 0;

        image : HTMLImageElement;
        anchoF: number;
        altoF: number;

        constructor(anchoF: number, altoF: number, x : number, y: number, image : HTMLImageElement) {
            this.x = x;
            this.y = y;
            this.image = image;
            this.anchoF = anchoF;
            this.altoF = altoF;
            
        }

        draw(ctx: CanvasRenderingContext2D): void {
            if (this.contador < this.retraso)
            {
                this.contador ++;
            }else
            {
                this.contador = 0;
                this.fotograma = this.fotograma < 3 ? this.fotograma + 1 : 0;
            }

            ctx.drawImage(this.image, this.fotograma * 32, 64, 32, 32, this.anchoF * this.x, this.altoF * this.y, this.anchoF, this.altoF);
        }

    }


    class Scene implements DrawableSimple {
        scene: number[][];

        anchoF: number;
        altoF: number;

        positionKey: Position;
        llaveEvent$: Subject<Position>;

        finishGame$: Subject<boolean>;

        image: HTMLImageElement;

        constructor(anchoF: number, altoF: number, tileMap: HTMLImageElement) {
            this.scene = [
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 2, 2, 0, 0, 0, 2, 2, 2, 2, 0, 0, 2, 2, 0],
                [0, 0, 2, 2, 2, 2, 2, 0, 0, 2, 0, 0, 2, 0, 0],
                [0, 0, 2, 0, 0, 0, 2, 2, 0, 2, 2, 2, 2, 0, 0],
                [0, 0, 2, 2, 2, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0],
                [0, 2, 2, 0, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 2, 0, 0, 0, 2, 2, 2, 0, 0, 2, 2, 2, 0],
                [0, 2, 2, 3, 0, 0, 2, 0, 0, 0, 1, 0, 0, 2, 0],
                [0, 2, 2, 2, 0, 0, 2, 0, 0, 2, 2, 2, 2, 2, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            ];

            this.anchoF = anchoF;
            this.altoF = altoF;

            this.image = tileMap;


            this.llaveEvent$ = new Subject<Position>();

            this.finishGame$ = new Subject<boolean>();

            this.llaveEvent$.subscribe(({ x, y }) => {
                this.scene[y][x] = 2;
                this.positionKey = {
                    x,
                    y
                }
            });

            this.finishGame$.subscribe(() => {
                this.scene[7][3] = 3;
            });
        }

        isCollission(x: number, y: number) {
            return this.scene[y][x] === 0;
        }

        draw(ctx: CanvasRenderingContext2D): void {
            from(this.scene).pipe(
                map((value, index) => ({
                    value,
                    index
                })),
                switchMap((s) =>
                    from(s.value).pipe(
                        map((value, x) => ({
                            value,
                            x,
                            y: s.index
                        }))
                    )
                ),
            )
                .subscribe(({ x, y, value }) => {
                    ctx.drawImage(this.image, value * 32, 0, 32, 32, this.anchoF * x, this.altoF * y, this.anchoF, this.altoF);
                });
        }

    };


    class MainCharacter implements Drawable {

        x: number;
        y: number;

        anchoF: number;
        altoF: number;

        scene: Scene;

        isKey: boolean;

        image: HTMLImageElement;

        position$ : Subject<Position>;


        constructor(anchoF: number, altoF: number, scene: Scene, image: HTMLImageElement) {
            this.anchoF = anchoF;
            this.altoF = altoF;
            this.x = 1;
            this.y = 1;
            this.scene = scene;
            this.image = image;
            this.position$ = new Subject();
        }

        reset() {
            this.x = 1;
            this.y = 1;
            this.isKey = false;
            this.scene.finishGame$.next(false);
        }

        validaJuego() {
            let value = this.scene.scene[this.y][this.x];

            if (value === 3) {
                this.isKey = true;
                this.scene.llaveEvent$.next({ x: this.x, y: this.y });
            } else if (value === 1) {
                if (this.isKey) {
                    this.reset();
                    this.scene.finishGame$.next(true);
                } else {
                    console.log('FALTA LLAVE');
                }
            }
        }



        up() {
            if (!this.scene.isCollission(this.x, this.y - 1)) {
                this.y--;
                this.position$.next({
                    x : this.x,
                    y : this.y
                })
                this.validaJuego();
            }
        }

        down() {
            if (!this.scene.isCollission(this.x, this.y + 1)) {
                this.y++;
                this.position$.next({
                    x : this.x,
                    y : this.y
                })

                this.validaJuego();
            }
        }

        left() {
            if (!this.scene.isCollission(this.x - 1, this.y)) {
                this.x--;
                this.position$.next({
                    x : this.x,
                    y : this.y
                })

                this.validaJuego();
            }
        }

        rigth() {
            if (!this.scene.isCollission(this.x + 1, this.y)) {
                this.x++;
                this.position$.next({
                    x : this.x,
                    y : this.y
                })

                this.validaJuego();

            }
        }


        draw(ctx: CanvasRenderingContext2D): void {
            ctx.drawImage(this.image, 32, 32, 32, 32, this.anchoF * this.x, this.altoF * this.y, this.anchoF, this.altoF);
        }

    }

    class Enemy implements Drawable {
        x: number;
        y: number;
        anchoF: number;
        altoF: number;
        scene: Scene;
        image: HTMLImageElement;
        direccion: number
        retraso: number;
        position$ : Subject<Position>;


        constructor(x: number, y: number, anchoF: number, altoF: number, scene: Scene, image: HTMLImageElement) {
            this.anchoF = anchoF;
            this.altoF = altoF;
            this.x = x;
            this.y = y;
            this.scene = scene;
            this.image = image;
            this.direccion = this.calcularDireccion();
            this.position$ = new Subject<Position>();
        }

        calcularDireccion(): number {
            return Math.floor(Math.random() * 4);
        }

        draw(ctx: CanvasRenderingContext2D): void {

            let { x, y, direccion } = this;

            if (this.retraso < 20) {
                this.retraso++;
            }
            else {
                switch (direccion) {
                    case 0:
                        y--;
                        break;
                    case 1:
                        y++;
                        break;
                    case 2:
                        x--;
                        break;
                    default:
                        x++;
                }

                if (!this.scene.isCollission(x, y)) {
                    this.x = x;
                    this.y = y;

                    this.position$.next({
                        x : this.x,
                        y : this.y
                    })
                }
                else {
                    let nva = direccion;
                    do {
                        nva = this.calcularDireccion();
                    } while (nva === direccion);

                    this.direccion = nva;

                }

                this.retraso = 0;
            }


            ctx.drawImage(this.image, 0, 32, 32, 32, this.anchoF * this.x, this.altoF * this.y, this.anchoF, this.altoF);

        }

    }

    const drawEnemies = (enemies: Enemy[], ctx: CanvasRenderingContext2D) => {
        enemies.forEach(s => s.draw(ctx));
    }



    const clear = (canvas: HTMLCanvasElement) => {
        canvas.width = 750;
        canvas.height = 500;
    };

    const tileMap = new Image();
    tileMap.src = 'assets/img/tilemap.png';

    const scene = new Scene(50, 50, tileMap);


    const main = new MainCharacter(50, 50, scene, tileMap);

    const enemies: Enemy[] = [
        new Enemy(3, 3, 50, 50, scene, tileMap),
        new Enemy(5, 7, 50 , 50 , scene, tileMap),
        new Enemy(7, 7, 50 , 50 , scene, tileMap),
    ];

    const antorcha = new Antorcha(50,50,0,0, tileMap);

    




    const moveMain = (key: string) => {
        if (key === 'ArrowUp') {
            main.up();
        } else if (key == 'ArrowDown') {
            main.down();
        } else if (key == 'ArrowRight') {
            main.rigth()
        } else {
            main.left();
        }
    }

    fromEvent<KeyboardEvent>(document, 'keydown').pipe(
        pluck('key'),
        filter(s => s === 'ArrowUp' || s === 'ArrowDown' || s === 'ArrowRight' || s === 'ArrowLeft'),
        tap(moveMain),
        map(() => ({
            x: main.x,
            y: main.y
        }))
    ).subscribe();


    const cicloPrincipal$ = interval(1000 / FPS);


    cicloPrincipal$.pipe(
        tap(() => {
            clear(canvas);
        }),
        tap(() => {
            scene.draw(ctx);
            main.draw(ctx);
            antorcha.draw(ctx);
        }),
        tap(() => {
            drawEnemies(enemies, ctx);
        })
    ).subscribe();

    const enemies$ = from(enemies).pipe(
        mergeMap(s => s.position$),
        filter( s => s.y === main.y && s.x === main.x)
    );

    const main$ = main.position$.pipe(
        filter(s => 
                enemies.filter( ({x,y}) => x === s.x && y === s.y).length > 0
            )
    );


    merge(enemies$, main$).subscribe((s)=>{
        main.reset();
    });
    







})();
