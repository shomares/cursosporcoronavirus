import IDrawable from "./IDrawable";

export default interface IDrawableDimensional extends IDrawable{
    heigth : number;
    width : number;
}