export default interface IMovimientoPlataforma{
    x : number;
    y : number;
    suelo : boolean;
    vy : number;
    vx : number
}