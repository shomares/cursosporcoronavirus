import IPosition from "./IPosition";

export default interface IDrawable extends IPosition{
    draw(ctx : CanvasRenderingContext2D) : void;
}