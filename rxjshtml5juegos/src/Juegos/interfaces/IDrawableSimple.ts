export default interface IDrawableSimple {
    draw(ctx : CanvasRenderingContext2D) : void;
}