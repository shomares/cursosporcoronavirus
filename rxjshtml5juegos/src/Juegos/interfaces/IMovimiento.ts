export default interface IMovimiento{
    x : number;
    y : number;
    angulo : number;
    ficha : number[][][];
    tipo : string

}