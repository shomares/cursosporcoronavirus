import { interval, fromEvent } from "rxjs";
import { tap, pluck, filter, map } from "rxjs/operators";

(() => {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext("2d");
    const FPS = 50;

    canvas.width = 500;
    canvas.height = 500;
    canvas.classList.add('canvasClasico');

    document.querySelector('body').append(canvas);

    interface Drawable {
        x: number;
        y: number;

        draw(ctx: CanvasRenderingContext2D): void;
    }

    class MainCharacter implements Drawable {

        x: number;
        y: number;
        velocity: number;
        image: HTMLImageElement;

        constructor(x: number, y: number) {
            this.x = x;
            this.y = y;

            this.image = new Image();
            this.image.src = 'assets/img/dino.png';
            this.velocity = 5;
        }

        up() {
            this.y -= this.velocity;
        }

        down() {
            this.y += this.velocity;
        }

        left() {
            this.x -= this.velocity;
        }

        rigth() {
            this.x += this.velocity;
        }


        draw(ctx: CanvasRenderingContext2D): void {
            ctx.drawImage(this.image, this.x, this.y, 50, 50);
        }

    }

    class Character implements Drawable {

        x: number;
        y: number;
        isDerecha: boolean;
        maxWidth: number;
        minWidth: number;

        constructor(x: number, y: number) {
            this.x = x;
            this.y = y;
            this.maxWidth = 400;
            this.minWidth = 20;
            this.isDerecha = true;
        }

        draw(ctx: CanvasRenderingContext2D): void {
            ctx.fillStyle = "#FF0000";
            ctx.fillRect(this.x, this.y, 50, 50);
        }

        moveX(velocity: number) {

            if (this.isDerecha) {
                if (this.x < this.maxWidth) {
                    this.x += velocity;
                } else {
                    this.isDerecha = false;
                }
            } else {
                if (this.x > this.minWidth) {
                    this.x -= velocity;
                } else {
                    this.isDerecha = true;
                }
            }
        }

    };


    const clear = (canvas: HTMLCanvasElement) => {
        canvas.width = 500;
        canvas.height = 500;
    };

    const move = (element: Character, velocity: number) => {
        element.moveX(velocity);
        element.draw(ctx);
    }

    let pers1 = new Character(10, 100);
    let pers2 = new Character(10, 200);
    let pers3 = new Character(10, 300);

    let main = new MainCharacter(10 , 50);


    const cicloPrincipal$ = interval(1000 / FPS);

    const moveMain = (key)=>{
        if (key === 'ArrowUp')
        {
            main.up();
        }else if (key == 'ArrowDown')
        {
            main.down();
        }else if (key == 'ArrowRight')
        {
            main.rigth()
        }else
        {
            main.left();
        }
    }

    fromEvent<KeyboardEvent>(document, 'keydown').pipe(
        pluck('key'),
        filter(s => s === 'ArrowUp' ||  s === 'ArrowDown'  || s === 'ArrowRight'  || s === 'ArrowLeft' ),
        tap(moveMain),
        map(()=> ({
            x : main.x,
            y : main.y
        }))
    ).subscribe(console.log);




    cicloPrincipal$.pipe(
        tap(() => {
            clear(canvas);
            move(pers1, 1);
            move(pers2, 5);
            move(pers3, 10);
        }),
        tap(()=>{
            main.draw(ctx);
        })
    ).subscribe();

})();
