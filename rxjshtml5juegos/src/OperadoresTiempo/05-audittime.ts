import { fromEvent } from "rxjs";
import { auditTime, map, tap } from "rxjs/operators";

const onClick$  = fromEvent<MouseEvent>(document, 'click');

onClick$.pipe(
    map( ({x}) => x),
    tap(val => console.log('tap: ', val)),
    auditTime(1000)
).subscribe(s=> console.log('next: ', s));
