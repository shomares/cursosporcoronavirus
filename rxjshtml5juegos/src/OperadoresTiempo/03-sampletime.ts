import { fromEvent } from "rxjs";
import { sampleTime, map } from "rxjs/operators";

const clickSource$ = fromEvent<MouseEvent>(document, 'click');

clickSource$.pipe(
    sampleTime(3000),
    map(({x,y}) => ({x, y}))
).subscribe(console.log); 