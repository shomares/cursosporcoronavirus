import { fromEvent } from "rxjs";
import { pluck, debounceTime, distinct } from "rxjs/operators";

const textbox = document.createElement('input');
document.querySelector('body').append(textbox);

const source$ = fromEvent<KeyboardEvent>(textbox , 'keydown');

source$.pipe(
    debounceTime(1000),
    pluck('target', 'value'),
    distinct()
).subscribe(console.log);
