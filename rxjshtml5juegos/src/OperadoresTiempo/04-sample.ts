import { interval, fromEvent } from "rxjs";
import { sample } from "rxjs/operators";

const button = document.createElement('button');
button.innerHTML = 'Stop it!'
document.querySelector('body').append(button);
const interval$ = interval(500);
const onclick$ = fromEvent(button, 'click');

interval$.pipe(
    sample(onclick$)
).subscribe(s=>{
    console.log(s);
})