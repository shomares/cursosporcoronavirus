import { fromEvent, asyncScheduler } from "rxjs";
import { pluck, throttleTime, distinct, skip, filter, distinctUntilChanged } from "rxjs/operators";

const textbox = document.createElement('input');
document.querySelector('body').append(textbox);

const source$ = fromEvent<KeyboardEvent>(textbox , 'keyup');

source$.pipe(
    pluck('target', 'value'),
    filter(s=> s !== ""),
    throttleTime(200, asyncScheduler, {
        leading : true,
        trailing : true
    }),
    distinct()
).subscribe(console.log);
