import { interval, of, forkJoin } from "rxjs";
import { take, delay } from "rxjs/operators";

const interval$ = interval(1000).pipe(
    take(3)
);

const letras$ = of('a', 'b' , 'c');

const numeros$ = of(1,2,3,4).pipe(
        delay(3500)
);


forkJoin({
    interval : interval$,
    letras : letras$,
    numeros : numeros$
}).subscribe(
    console.log
);

