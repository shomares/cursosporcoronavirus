import { of, concat, interval } from "rxjs";
import { take } from "rxjs/operators";

const source1$ = of(1,2,3,4,9);
const source2$ = of('a', 'b', 'z');
const interval$ = interval(1000);

concat(
        source1$,
        source2$,
        interval$.pipe(
            take(8)
        ),
        [7,8]
    ).subscribe(console.log);