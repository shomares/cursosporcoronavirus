import { of } from "rxjs";
import { startWith, endWith } from "rxjs/operators";

const source$ = of(1,2,3,4);

source$.pipe(
    startWith('a', 'b', 'c'),
    endWith('x', 'y', 'z')
).subscribe(console.log);