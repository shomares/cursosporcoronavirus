import { fromEvent, combineLatest, Observable } from "rxjs";
import { pluck } from "rxjs/operators";

const userInput = document.createElement('input');
const passwordInput = document.createElement('input');

userInput.placeholder = "usuario";
passwordInput.placeholder = "password";
passwordInput.type = "password";

document.querySelector('body').append(userInput, passwordInput);

const getEvent = (element : HTMLElement) : Observable<string> =>
    fromEvent<KeyboardEvent>(element, 'keyup')
    .pipe(
        pluck<KeyboardEvent, string>('target', 'value')
    );

combineLatest(
        getEvent(userInput),
        getEvent(passwordInput)
).subscribe(console.log);

