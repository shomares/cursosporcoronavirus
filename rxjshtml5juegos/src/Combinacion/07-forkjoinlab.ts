import { forkJoin, of } from "rxjs";
import { ajax } from "rxjs/ajax";
import { catchError } from "rxjs/operators";

const api =  'https://api.github.com/users';
const user = 'klerith';

forkJoin(
    {
        profile : ajax.getJSON(`${api}/${user}`),
        repos : ajax.getJSON(`${api}/${user}/reposa`)
                    .pipe(catchError(e=> of(e.message))),
        gists : ajax.getJSON(`${api}/${user}/gists`)
    }
).subscribe(console.log);