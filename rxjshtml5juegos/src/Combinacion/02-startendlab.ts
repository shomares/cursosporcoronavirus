import { ajax } from "rxjs/ajax";
import { startWith, switchMap } from "rxjs/operators";
import { fromEvent } from "rxjs";

const body = document.querySelector('body');
const divLoading = document.createElement('div');
const button = document.createElement('button');

divLoading.classList.add('loading');
divLoading.innerHTML = 'Cargando...';
button.innerHTML = 'Consulta';

body.append(button);

const clickButton$ = fromEvent<MouseEvent>(button, 'click');
const $source = ajax.getJSON('https://reqres.in/api/users/2?delay=2');

clickButton$.pipe(
    switchMap(() => $source.pipe(
                                    startWith(true)
                                )
            ),
).subscribe(s => {
    if (s === true) {
        body.append(divLoading);
    } else {
        document.querySelector('.loading').remove();
    }

    console.log(s);


});



