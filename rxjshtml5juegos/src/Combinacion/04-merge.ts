import {  merge, fromEvent } from "rxjs";
import { pluck } from "rxjs/operators";

const clickEvent$ = fromEvent(document, 'click');
const keyEvent$ = fromEvent(document, 'keyup');

merge(
    clickEvent$,
    keyEvent$
).pipe(
    pluck('type')
).subscribe(console.log);



