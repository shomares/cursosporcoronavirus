import {timer, interval} from 'rxjs';

const intervalObs$ = interval(1000);

const timerSimple$ = timer(2000);

const timerlikeInterval$ = timer(2000, 1000);

const date = new Date();
date.setSeconds(date.getSeconds() + 5 );

const timerconDate$ = timer(date);


//intervalObs$.subscribe(s=> console.log('interval ', s));
//timerSimple$.subscribe(s=> console.log('simple ', s));
//timerlikeInterval$.subscribe(s=> console.log('timerlikeInterval ', s));
timerconDate$.subscribe(s=> console.log('timerconDate ', s));


