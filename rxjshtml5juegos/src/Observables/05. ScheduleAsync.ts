import {asyncScheduler, fromEvent, Subscription} from 'rxjs';

asyncScheduler.schedule(()=>console.log('Hola soy un timer simple'), 1000);

asyncScheduler.schedule((data)=>console.log('Hola soy un timer con estado', data), 100, 'mapaches');


let schedule :Subscription = null;

const buttonStart = document.createElement('button');
const button = document.createElement("button");
const result = document.createElement('p');

buttonStart.innerHTML = "Start";
button.innerHTML = "Stop";


document.body.appendChild(buttonStart);
document.body.appendChild(button);

document.body.appendChild(result);

const buttonStartclick$ = fromEvent(buttonStart, 'click');
buttonStartclick$.subscribe(()=>{
    if (!schedule)
    {
        schedule = asyncScheduler.schedule(function(data: number){
            console.log('Hola soy un interval', data++);
            result.innerHTML = 'Hola soy un interval' + data;
            this.schedule(data, 1000);
        }, 0, 1);
    }
});


const buttonclick$  = fromEvent(button, 'click');
buttonclick$.subscribe(s=>{
    if (schedule)
    {
        schedule.unsubscribe();
        result.innerHTML = null;
        schedule = null;
    }
});

