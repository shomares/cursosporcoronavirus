import { Observable } from 'rxjs';

const timer$ = new Observable<number>(subscriber => {
    let count = 0;
    const handler = setInterval(() => {
        subscriber.next(count++)
        console.log('continuo');
    }, 1000);

    return ()=> clearInterval(handler);
});

const subs = timer$.subscribe(console.log);

setTimeout(()=>{
    subs.unsubscribe();
    console.log('unsubscribe');
}, 3000)









