import {fromEvent} from 'rxjs';

const clickobservable$ = fromEvent<MouseEvent>(document, 'click');
const keydownobservable$ = fromEvent<KeyboardEvent> (document, 'keydown');

clickobservable$.subscribe(({x,y})=> console.log(x, y));
keydownobservable$.subscribe(({key})=> console.log(key));