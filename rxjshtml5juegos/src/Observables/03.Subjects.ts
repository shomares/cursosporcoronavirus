import { Observable, Observer, Subject } from 'rxjs';

const observer: Observer<number> = {
    next: (value) => console.log('[next]' + value),
    complete: () => console.log('Completo'),
    error: () => console.warn
};

const timer$ = new Observable<number>(subscriber => {

    const interval = setInterval(() => {
        subscriber.next(Math.random());
    }, 1000);

return () => {
    clearInterval(interval);
    console.log('Interval destruido');
}
});

const subject$ = new Subject<number>();
const subscription = timer$.subscribe(subject$);

const subs1 = subject$.subscribe(observer);
const subs2 = subject$.subscribe(observer);
const subs3 = subject$.subscribe(observer);

setInterval(() => {
    console.log('From subject');
    subject$.next(200);
    subject$.complete();
    subscription.unsubscribe();
}, 4000);









