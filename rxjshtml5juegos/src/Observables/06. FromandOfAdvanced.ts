import { of, from} from 'rxjs';

const observer = {
    next : (data)=> console.log('next' + data)
};

const source$ = from([1,2,3,4,5]);

const generator = function *(){
    for(let i=0; i<20; i++){
        yield Math.random();
    }
};

console.log('Soy sincrono');
const generate = generator();

const sourceFromGenerator$ = from(generate);

source$.subscribe(observer);
sourceFromGenerator$.subscribe(observer);


const sourceOf$ = of(1,2,3,4,5);
sourceOf$.subscribe(observer);

console.log('adios');


const sourceoffetch$ = from(fetch('https://jsonplaceholder.typicode.com/todos/1')); 

sourceoffetch$.subscribe(async val=>{
    console.log(val);
    let result = await val.json();
    console.log(result);

})

