import { Observable, Observer } from 'rxjs';

const observer : Observer<number> = {
    next : (value) => console.log('[next]' + value),
    complete : ()=> console.log('Completo'),
    error: ()=> console.warn
}

const timer$ = new Observable<number>(subscriber => {
    let count = 0;
    const handler = setInterval(() => {
        subscriber.next(count++)
        console.log('continuo');
    }, 1000);

    setTimeout(()=>{
        subscriber.complete()
    }, 3000);

    return ()=> clearInterval(handler);
});

const subs1 = timer$.subscribe(observer);
const subs2 = timer$.subscribe(observer);
const subs3 = timer$.subscribe(observer);

subs1.add(subs2)
     .add(subs3);



setTimeout(()=>{
    subs1.unsubscribe();
    console.log('unsubscribe');
}, 6000)









