import {interval, timer} from 'rxjs';

const intervalObs$ = interval(1000);

const timerSimple$ = timer(1000);

const timerlikeinterval$ = timer(1000, 2000);

let date = new Date();
date.setSeconds(date.getSeconds() +  5);

const timeDate$ = timer(date);

intervalObs$.subscribe(s=> console.log('From interval' + s));
timerSimple$.subscribe(s=> console.log('From timer simple' + s));

timerlikeinterval$.subscribe(s=> console.log('From timer like subscribe' + s));
timeDate$.subscribe(s=> console.log('From time'));