import { GitUser } from "./GitUser";

export interface GitUsersResp {
    total_count:        number;
    incomplete_results: boolean;
    items:              GitUser[];
}

