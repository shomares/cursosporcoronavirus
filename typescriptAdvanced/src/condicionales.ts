type DarkColors = "Black" | "More black"
type WhiteColors = "Blue" | "Yellow"

type Status = "happy" | "sad"

type Pallete<T extends Status> =  T extends "happy"  ? WhiteColors : DarkColors

const PalleteOne : Pallete<"happy"> = "Blue"
const PalleteTwo : Pallete<"sad"> = "Black"

