type Gender = "Man" | "Woman"
type LabourDay = "Lunes" | "Martes" | "Miercoles" | "Jueves" | "Viernes"


interface Worker {
    gender : Gender,
    daysToWork : LabourDay[]
}

export default  () => {
    const worker  : Worker = {
        daysToWork : ["Jueves", "Lunes"],
        gender : "Man"
    }

    const worker2 : Worker = {
        gender : "Woman",
        daysToWork : ["Lunes"]
    }

    console.table(worker)
    console.table(worker2)
}