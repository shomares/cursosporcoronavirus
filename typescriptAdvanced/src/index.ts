import alias from './alias'
import interception from './intercepciones'
import {FirstWay, SecondWay} from './unions'
import TemplateUnions from './templateunions'
import KeyOf from './keyof'
import Mapped from './mappedType'
import ReadOnly from './readonly'
import Partial from './partial'
import Exclude from './excludeandextract'

(()=> {
    alias()
    interception()

    FirstWay()
    SecondWay()

    TemplateUnions()
    KeyOf()
    Mapped()

    ReadOnly()

    Partial()

    Exclude()

})()