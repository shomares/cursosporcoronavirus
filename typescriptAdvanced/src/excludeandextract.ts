interface Student {
    name : string,
    edad : number,
    calificacion: number
}

interface Empleado{
    name : string,
    edad : number,
    salario : number
}

type Diff<T extends Object, U extends Object> = {
    [P in Exclude<keyof T, keyof U>] 
}

type Common <T extends Object, U extends Object> = {
    [P in Extract<keyof T, keyof U>]
}

const diffObject  = <T extends Object, U extends Object>(value : Readonly<T>, obj : Readonly<U>) : Common<T, U> => {
    let valueNvo = {... value}
    for (const key in value) {
        if (value.hasOwnProperty(key) && !obj.hasOwnProperty(key)) {
            delete valueNvo[key]
        }
    }

    return valueNvo
}

export default ()=> {
    const student : Student = {
        calificacion : 12,
        edad: 12,
        name: "Test"
    }

    const persona : Empleado = {
        edad : 12,
        name : "Test",
        salario : 1200
    }

    let diffValue = diffObject(student, persona)
    console.log(diffValue.edad)

    



}