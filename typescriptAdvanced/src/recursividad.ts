///recursividad con interfaces
interface ITreeNode<T> {
    value : T;
    children? : ITreeNode<T>[]
}

//Recursividad con type y interface
type TreeNode<T> =  T & {next? : TreeNode<T>}
interface Student {
    name : string;
    calificacion : number
}

//Recusrividad con type --revisar
type TreeNodeAgain<T> = T | [TreeNodeAgain<T>]


const treeNode : ITreeNode<number> = {
    value : 1,
    children : [
        {
            value : 2,
            children : [
                {
                    value : 1
                }
            ]
        },
        {
            value : 3
        }
    ]
}

let treeNode2 : TreeNode<Student> 
treeNode2.name = "Juanito"

