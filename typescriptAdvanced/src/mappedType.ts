
interface ProductItem {
    name: string,
    price: number
}

type Envolver<T> = {
    [Key in keyof T]: (arg: T[Key]) => T[Key]
}


const envolveProduct: Envolver<ProductItem> = {
    name: (name: string) => name.trim(),
    price: (value: number) => value * 1.16
}

const envolve = <T>(obj: T, envolver: Envolver<T>): T => {

    var keys = Object.keys(obj)
    let value = {}

    keys.forEach(key => {
        const originalValue = obj[key]
        const transformValue = envolver[key](originalValue)
        value[key] = transformValue

    })

    return <T> value
}

export default () => {
    const item: ProductItem = {
        name: "  Mac Serie pro  ",
        price: 1200
    }

    const transform = envolve(item, envolveProduct)

    console.log(transform)

}