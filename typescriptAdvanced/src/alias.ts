//Alias----------------------------
type FunctionReducer<S> = (antValue: S, current: S) => S

type Wrapper<T> = {
    value: T
    message: string
}

type ReducerWrapper<S>  = FunctionReducer<Wrapper<S[]>>

const testFunction: ReducerWrapper<number> = (antValue: Wrapper<number[]>, current: Wrapper<number[]>) => {
    return {
        value: [...antValue.value, ...current.value],
        message: "Test"
    }
}

export default ()=> {
    const value = testFunction( {
        message : "OK",
        value : [1, 2, 3]
    }, {
        message : "OK2",
        value : [3, 4, 5]
    })
    
    
    console.table(value)
}
