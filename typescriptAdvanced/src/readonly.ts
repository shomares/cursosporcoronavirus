
const unshiftInmuttable = <T>(array : Readonly<T> [] ) : T[] => {
    const [, ...tail] = array || []
    return tail
}

const shiftMuttable = <T> (array : T[]) : T[] => (array.shift(), array)

export default () => {
    let array1 = [1, 3, 4, 6 ,5]
    console.log('Muttable', array1, shiftMuttable(array1))
    let array2 = [1, 3, 4, 6 ,5]
    console.log('Inmuttable', array2, unshiftInmuttable(array1))
    

}