interface User {
    username: string;
    isPremium: boolean;
    posts: number
}

const createState = <T>(initialState: Readonly<T>): {
    setState: (partial: Partial<T>) => T
} => {
    let initial: T = initialState

    return {
        setState: (partial: Partial<T>): T => (
            initial = { ...initial, ...partial }
        )
    }
}

export default () => {
    const initialUser: User = {
        username: "emejia",
        isPremium: false,
        posts: 1
    }

    console.log(initialUser)

    const { setState } = createState(initialUser)

    console.log(setState({
        posts: 10
    }))

    console.log(setState({
        posts: 20,
        isPremium : true
    }))


}