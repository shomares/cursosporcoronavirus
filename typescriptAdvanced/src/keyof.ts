interface Week {
    lunes : string,
    martes : string,
    miercoles : string,
    jueves : string,
    viernes : string
}

type ValuesWeek = keyof(Week)

const algo : ValuesWeek = "lunes"





const showProp = <T>(data : T, ...values : (keyof T) [] ) : void => {
    values.forEach( key => 
            console.log(data[key])
        )
}

export default ()=> {
    const developer = {
        nombre : "Eneas",
        languages : ["js", "ts", "c#"],
        isSenior : false
    }

    showProp(developer, "isSenior", "nombre", "languages")

}



