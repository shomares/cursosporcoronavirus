const compose = <A,B> (a : A, b : B): A & B => ({
    ...a,
    ...b
})

export default () => {
    const unionValue = compose({
        nombre : "Eneas",
        edad : 14
    }, {
        profesion : "Developer",
        nacionalidad : "Mexicana"
    })

    const unionValue2 = compose({
        nombre : "Gatito",
        esBonito : true
    }, {
        edad : 2
    })

    console.log(unionValue.edad)
    console.log(unionValue2.nombre)


}