interface HumanBeing {
    sleep: () => void
}

interface Man extends HumanBeing {
    mustache: boolean
}

interface Woman extends HumanBeing {
    longHair: boolean
}

const getRandomValue = (): boolean => Math.random() === 0

const getMan = (): Man => ({
    mustache: getRandomValue(),
    sleep: () => console.log('slepp man z z z')
})

const getWoman = (): Woman => ({
    sleep: () => console.log('slepp woman z z z'),
    longHair: getRandomValue()
})

const getHumanBeing = (): Man | Woman =>
getRandomValue() ? getMan() : getWoman()



export const FirstWay = () => {
    const human = getHumanBeing()

    if ("mustache" in human) {
        console.log('Is a man: ', human.mustache)
    } else {
        console.log('Is a woman: ', human.longHair)
    }
}

const isMan = (human: Man | Woman): human is Man =>
    (<Man>human).mustache !== undefined

export const SecondWay = () => {
    const human = getHumanBeing()

    if (isMan(human)) {
        console.log('Is a man second way: ', human.mustache)
    } else {
        console.log('Is a woman second way: ', human.longHair)
    }

}
